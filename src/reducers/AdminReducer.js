import { ADMIN_UPDATE_USER, USERS } from '../actions/ActionTypes';
import initialState from './InitialState';

/**
 * Message reducer
 *
 * @export
 * @param {Object} [state=initialState.users] initial state
 * @param {Object} action action
 * @returns {Object} reduced or initial state
 */
export default (state = initialState.users, action) => {
  switch (action.type) {
    case USERS:
      return {
        users: action.payload.data.users,
        pagination: {
          currentCount: action.payload.data.current_count,
          currentPage: action.payload.data.current_page,
          nextUrl: action.payload.data.next_url,
          prevUrl: action.payload.data.prev_url,
          totalUsers: action.payload.data.total_users,
          totalPages: action.payload.data.total_pages
        }
      };

    case ADMIN_UPDATE_USER: {
      const { payload, type } = action;
      const userToUpdate = state.find((user) => user.id === payload.id);
      const remainingUsers = state.filter((user) => user.id !== payload.id);
      const updatedUser = [{ ...userToUpdate, ...payload }];
      const updatedUsersList = updatedUser.concat(remainingUsers);
      return updatedUsersList;
    }

    default:
      return state;
  }
};
