import reduceReducers from 'reduce-reducers';
import { combineReducers } from 'redux';

import { authUser, user } from './UserReducer';
import devotionals from './DevotionalReducer';
import homepage from './HomepageReducer';
import message from './MessageReducer';
import { nodes, node } from './NodeReducer';
import users from './AdminReducer';

const combinedReducers = combineReducers({
  authUser,
  devotionals,
  homepage,
  message,
  nodes,
  node,
  user,
  users
});

export default reduceReducers(combinedReducers);
