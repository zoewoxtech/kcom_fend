export default {
  authUser: null,
  devotional: null,
  devotionals: null,
  homepage: null,
  message: null,
  nodes: null,
  node: null,
  users: null,
  user: null,
  recentDevo: null
};
