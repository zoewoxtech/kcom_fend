import { DEVOTIONALS } from '../actions/ActionTypes';
import initialState from './InitialState';

/**
 * Message reducer
 *
 * @export
 * @param {Object} [state=initialState.devotionals] initial state
 * @param {Object} action action
 * @returns {Object} reduced or initial state
 */
export default (state = initialState.devotionals, action) => {
  switch (action.type) {
    case DEVOTIONALS:
      return {
        nodes: action.payload.nodes,
        nodetype: action.payload.nodetype
      };

    default:
      return state;
  }
};
