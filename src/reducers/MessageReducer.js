import { SUCCESS_MESSAGE, ERROR_MESSAGE } from '../actions/ActionTypes';
import initialState from './InitialState';

/**
 * Message reducer
 *
 * @export
 * @param {Object} [state=initialState.message] initial state
 * @param {Object} action action
 * @returns {Object} reduced or initial state
 */
export default (state = initialState.message, action) => {
  switch (action.type) {
    case SUCCESS_MESSAGE:
      return action.successMessage;

    case ERROR_MESSAGE:
      return action.errorMessage;

    default:
      return state;
  }
};
