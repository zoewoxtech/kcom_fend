import { WELCOME_MESSAGE } from '../actions/ActionTypes';
import initialState from './InitialState';

/**
 * Message reducer
 *
 * @export
 * @param {Object} [state=initialState.homepage] initial state
 * @param {Object} action action
 * @returns {Object} reduced or initial state
 */
export default (state = initialState.homepage, action) => {
  switch (action.type) {
    case WELCOME_MESSAGE:
      return action.payload.data.node;

    default:
      return state;
  }
};
