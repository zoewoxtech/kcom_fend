import {
  CREATE_NODE, NODE, NODES, RESET_NODE
} from '../actions/ActionTypes';
import initialState from './InitialState';

/**
 * Nodes reducer
 *
 * @export
 * @param {Object} [state=initialState.nodes] initial state
 * @param {Object} action action
 * @returns {Object} reduced or initial state
 */
export const nodes = (state = initialState.nodes, action) => {
  switch (action.type) {
    case NODES:
      return {
        nodes: action.payload.data.nodes,
        nodetype: action.payload.nodetype,
        pagination: {
          perPage: action.payload.data.perPage,
          currentCount: action.payload.data.current_count,
          currentPage: action.payload.data.current_page,
          nextUrl: action.payload.data.next_url,
          prevUrl: action.payload.data.prev_url,
          totalNodes: action.payload.data.total_nodes,
          totalPages: action.payload.data.total_pages
        }
      };

    case CREATE_NODE: {
      const { payload } = action;
      const _newNodes = [{ ...payload }];
      const newNodesList = _newNodes.concat(state);
      return state;
    }

    default:
      return state;
  }
};

/**
 * Node reducer
 *
 * @export
 * @param {Object} [state=initialState.node] initial state
 * @param {Object} action action
 * @returns {Object} reduced or initial state
 */
export const node = (state = initialState.node, action) => {
  switch (action.type) {
    case NODE:
      return action.payload.node;

    case RESET_NODE:
      return null;

    default:
      return state;
  }
};
