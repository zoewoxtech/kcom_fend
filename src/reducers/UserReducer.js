import { LOGGEDIN_USER, SIGNOUT_USER, USER } from '../actions/ActionTypes';
import { checkAuth } from '../utils/helper';
import initialState from './InitialState';

/**
 * Message reducer
 *
 * @export
 * @param {Object} [state=initialState.message] initial state
 * @param {Object} action action
 * @returns {Object} reduced or initial state
 */
export const authUser = (state = checkAuth(), action) => {
  switch (action.type) {
    case LOGGEDIN_USER:
      return action.payload;

    case SIGNOUT_USER:
      return {
        user: null
      };

    default:
      return state;
  }
};

export const user = (state = initialState.user, action) => {
  switch (action.type) {
    case USER:
      return action.user;

    default:
      return state;
  }
};
