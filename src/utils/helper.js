import axios from 'axios';
import jwtDecode from 'jwt-decode';

/**
 * Set the a value inside localStorage
 * @export
 * @param {String} key - key to store value with in localStorage
 * @param {String} value - item to store inside localStorage
 * @returns {Promise} returns a promise and resolves it with the stored value
 */
export const setLocalstorage = (key, value) => {
  let storedValue;
  return new Promise((resolve) => {
    if (window.localStorage.getItem(key)) {
      window.localStorage.removeItem(key);
    }
    window.localStorage.setItem(key, value);
    storedValue = window.localStorage.getItem(key);
    if (storedValue) {
      resolve(storedValue);
    }
  });
};

/**
 * clear or remove one or all items from localStorage
 * @export
 * @param {String} key - item to remove in localStorage
 * @returns {Void} does not have a return value
 */
export const clearLocalStorage = (key) => {
  if (!key) {
    window.localStorage.clear();
  } else {
    window.localStorage.removeItem(key);
  }
  axios.defaults.headers.common.Authorization = '';
  delete axios.defaults.headers.common.Authorization;
};

/**
 * set a default header in axios
 * @export
 * @param {String} token - value to set header as
 * @returns {Void} does not have a return value
 */
export const setDefaultHeader = (token) => {
  axios.defaults.headers.common.Authorization = '';
  delete axios.defaults.headers.common.Authorization;

  if (token) {
    axios.defaults.headers.common.Authorization = token;
    axios.defaults.headers.common['Allow-Control-Allow-Origin'] = '*';
  }
};

/**
 * Checks if the token is set and updates state accordingly
 * @export
 * @returns {*} state
 */
export const checkAuth = () => {
  const token = window.localStorage.getItem('kcomjwt');
  if (token) {
    const decodedToken = jwtDecode(token);
    if (decodedToken) {
      if (decodedToken.exp < Date.now() / 1000) {
        window.localStorage.removeItem('kcomjwt');
        return null;
      }
      return decodedToken.UserInfo;
    }
    window.localStorage.removeItem('kcomjwt');
    return null;
  } else {
    window.localStorage.removeItem('kcomjwt');
    return null;
  }
};

export const adminAuth = () => {
  const token = window.localStorage.getItem('kcomjwt');
  if (token) {
    const decodedToken = jwtDecode(token);
    if (decodedToken) {
      const acceptedRoles = ['super_admin', 'admin'];
      const userRole = decodedToken.UserInfo.role;
      if (acceptedRoles.indexOf(userRole) === -1) {
        return false;
      }
      return true;
    }
  }
};

export const jsUcFirst = (str) => {
  const newString = str.charAt(0).toUpperCase() + str.slice(1);
  return newString;
};

export const truncateText = (str, length = 100, ending = '...') => {
  if (str.length > length) {
    return str.substr(0, length - ending.length) + ending;
  } else {
    return str;
  }
};
