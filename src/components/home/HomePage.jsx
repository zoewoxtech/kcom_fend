import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import renderHTML from 'react-render-html';

import Carousel from '../common/Carousel';
import { Loader } from '../common/Loader';
import Sidebar from '../common/Sidebar';
import RecentBlogs from './RecentBlogs';
import RecentCC from '../common/RecentCC';

import { getOneNode, getNodes } from '../../actions/HomeActions';
import { getDevotionals } from '../../actions/DevotionalActions';

/**
 * @class HomePage
 * @extends {Component}
 */
export class HomePage extends Component {
  /**
   * Creates an instance of AllUsers.
   * @param {any} props property of component
   * @param {any} context property of component
   * @returns {*} no return value
   * @memberof AllUsers
   */
  constructor(props, context) {
    super(props, context);

    this.state = {
      hasErrors: false
    };
  }

  /**
   * @desc handles the triggering of the necessary action
   * @returns {null} returns no value
   */
  componentWillMount() {
    const { limit, sort, order } = this.state;
    this.props.getOneNode('page', 'welcome-kcom');
    this.props.getDevotionals(
      'daily_devotional',
      '', 4, 1, 'publish_date', 'desc'
    );
    this.props.getNodes('blog', '', 3, 1, 'created_at', 'desc');
  }

  /**
   * renders the HomePage component
   * @returns {Object} react component to render
   * @memberOf HomePage
   */
  render() {
    const {
      recentDevotionals, homepageContent, message, nodes
    } = this.props;

    if (_.isEmpty(recentDevotionals) || _.isEmpty(nodes)
      || _.isEmpty(homepageContent)) {
      return <Loader />;
    } else if (this.state.hasErrors) {
      return (
        <div>
          <h6>{message}</h6>
        </div>
      );
    } else if (homepageContent) {
      const content = <div className="col-lg-9 col-sm-12">
        <h2>{ homepageContent.title }</h2>
        { renderHTML(homepageContent.content) }
      </div>;

      return (
        <div>
            <Carousel/>
            <div className="container main-content">
              <div className="row row-spacing">
                {content}
                <aside className="col-sm-12 col-lg-3 blog-sidebar">
                  <Sidebar />
                </aside>
              </div>
              <div className="divider">
              </div>
              <div className="row row-spacing">
                <RecentBlogs nodes={nodes} />
              </div>
              <div className="divider">
              </div>
              <div className="row row-spacing">
                <RecentCC recentDevotionals={recentDevotionals} />
              </div>
            </div>
        </div>
      );
    }
  }
}

/**
 *  map state to props
 *
 * @param {state} state
 * @returns {*} props
 */
const mapStateToProps = ({
  devotionals = [], homepage, message, nodes = []
}) => ({
  homepageContent: homepage,
  recentDevotionals: devotionals,
  message,
  nodes
});

export default connect(mapStateToProps, {
  getDevotionals, getNodes, getOneNode
})(HomePage);
