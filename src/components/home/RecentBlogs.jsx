import React from 'react';
import { Link } from 'react-router-dom';
import { jsUcFirst, truncateText } from '../../utils/helper';

import { LoaderSmall } from '../common/Loader';

const RecentBlogs = ({ nodes }) => {
  const recentNode = nodes.nodes.map(node =>
    <div className="col-lg-4" key={node.id}>
      <h5 className="px-2">
        <Link to={`/${nodes.nodetype}/${node.id}`}>
          {truncateText(jsUcFirst(node.title), 35)}
        </Link>
      </h5>
      <p className="text-justify py-2 px-2">
        {truncateText(node.content, 200)}
      </p>
      <Link className="float-right"
        to={`/${nodes.nodetype}/${node.id}`}>
        Read More...
      </Link>
    </div>);

  return (
    <div className="col-lg-12">
      <div className="row">
        <h4 className="col-lg-11">Recent Blog Posts</h4>
        <Link className="btn btn-outline-primary float-right"
          to="">
          View All
        </Link>
      </div>
      <div className="row">
        {recentNode}
      </div>
    </div>
  );
};

export default RecentBlogs;
