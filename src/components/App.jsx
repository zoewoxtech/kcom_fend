import React, { Component } from 'react';
import { Route } from 'react-router-dom';

import AllUsers from './admin/AllUsers';
import AllNodes from './admin/AllNodes';
import Contact from './nodes/Contact';
import CreateNode from './nodes/CreateNode';
import Dashboard from './users/Dashboard.jsx';
import EditNode from './nodes/EditNode';
import ErrorPage from './common/ErrorPage';
import Login from './users/Login';
import Nodes from './nodes/Nodes';
import Pages from './nodes/Pages';
import Signup from './users/Signup';
import ViewNode from './nodes/ViewNode';

/**
 * Component to persist across all routes
 * @class App
 * @extends {Component}
 */
const App = () => <div>
  <main role="main">
    {/* DEFAULT */}
    <Route exact path="/contact-us" component={Contact} />
    <Route exact path="/error" component={ErrorPage} />

    {/* NODES */}
    <Route exact path="/nodes/:nodeType" component={AllNodes} />
    <Route exact path="/article/:nodeType" component={CreateNode} />
    <Route exact path="/edit/:nodeType/:nodeId" component={EditNode} />
    <Route exact path="/articles/:pageName" component={Nodes} />
    <Route exact path="/page/:pageName" component={Pages} />
    <Route exact path="/articles/:nodeType/:nodeId" component={ViewNode} />

    {/* USERS */}
    <Route exact path="/users" component={AllUsers} />
    <Route exact path="/dashboard" component={Dashboard} />
    <Route exact path="/login" component={Login} />
    <Route exact path="/signup" component={Signup} />
  </main>
</div>;

export default App;
