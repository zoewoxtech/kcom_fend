import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import { compose } from 'redux';
import toastr from 'toastr';

import AdminNavbar from '../admin/Navbar';

import { logout } from '../../actions/UserActions';
import { adminAuth } from '../../utils/helper';

/**
 * @class NavBar
 * @extends {Component}
 */
class NavBar extends Component {
  /**
   * Creates an instance of Navbar.
   * @param {any} props property of component
   * @param {any} context property of component
   * @returns {*} no return value
   * @memberof Navbar
   */
  constructor(props, context) {
    super(props, context);

    this.logout = this.logout.bind(this);
  }

  /**
   * @desc Log out a user
   * @param {any} event
   * @returns {null} returns no value
   */
  logout(event) {
    event.preventDefault();
    this.props.logout()
    .then(() => {
      toastr.success(this.props.message);
      this.props.history.push('/');
    });
  }

  /**
   * renders the NavBar component
   * @returns {Object} react component to render
   * @memberOf NavBar
   */
  render() {
    const { authUser, user } = this.props;
    return (
      <div>
        { adminAuth() ? <AdminNavbar /> : ''}
        {/* White nav bar */}
        <nav className="navbar navbar-expand-sm navbar-white container-fluid">
          <div className="container nav-white-container">
            <ul className="nav ml-auto nav-icon-ul">
              <li className="inner-addon right-addon nav-item nav-item-white">
                <input
                  type="text"
                  className="form-control nav-search" placeholder="search"
                />
              </li>
              <li className="nav-item nav-item-white dropdown">
                <i
                  className="fas fa-user-circle
                    fa-lg nav-icon nav-user-icon dropbtn"
                  aria-hidden="true"
                >
                </i>
                <div className="dropdown-content">
                  { authUser
                  ? <span>
                      <Link className=""
                        to="/dashboard">
                        Dashboard
                      </Link>
                      <Link className=""
                        to="/logout" onClick={this.logout}>
                        Logout
                      </Link>
                    </span>
                  : <span>
                      <Link className=""
                      to="/login">
                      Login
                    </Link>
                    <Link className=""
                      to="/signup">
                      Signup
                    </Link>
                  </span> }
                </div>
              </li>
            </ul>
            <img
              src='../../../public/images/kcom-logo.png'
              alt='kcom logo'
              id='kcom-logo'
            />
            <div className='kcom-nav-text'>
              <p id='kcom-nav-first-text'>Kingdom Capstone</p>
              <p id='kcom-nav-second-text'>Outreach Ministry</p>
            </div>
          </div>
        </nav>

        {/* Blue nav bar */}
        <nav className="navbar navbar-expand-sm navbar-blue container-fluid">
          <div className="container">
            <ul className="nav row">
              <li className="nav-item nav-item-blue active">
                <Link className="nav-link nav-link-blue"
                  to="/">
                  Home
                </Link>
              </li>
              <li className="nav-item nav-item-blue dropdown">
                <a className="nav-link nav-link-blue dropdown-toggle" href="#"
                  id="navbarDropdown" role="button" data-toggle="dropdown"
                  aria-haspopup="true" aria-expanded="false" >
                  About KCOM
                </a>
                <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <Link className="dropdown-item"
                    to="/page/who-we-are">
                    Who We Are
                  </Link>
                  <div className="dropdown-divider"></div>
                  <Link className="dropdown-item"
                    to="/page/ministry-team">
                    Ministry Team
                  </Link>
                  <div className="dropdown-divider"></div>
                  <Link className="dropdown-item"
                    to="/page/ministry-arms">
                    Ministry Arms
                  </Link>
                </div>
              </li>
              <li className="nav-item nav-item-blue">
                <a className="nav-link nav-link-blue" href="#">Events</a>
              </li>
              <li className="nav-item nav-item-blue dropdown">
                <a className="nav-link nav-link-blue dropdown-toggle" href="#"
                  id="navbarDropdown" role="button" data-toggle="dropdown"
                  aria-haspopup="true" aria-expanded="false" >
                  Articles
                </a>
                <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <Link className="dropdown-item"
                    to="/articles/blog">
                    Blogs
                  </Link>
                  <div className="dropdown-divider"></div>
                  <Link className="dropdown-item"
                    to="/articles/daily_devotional">
                    Capstone Capsules
                  </Link>
                </div>
              </li>
              <li className="nav-item nav-item-blue">
                <a className="nav-link nav-link-blue" href="#">Books</a>
              </li>
              <li className="nav-item nav-item-blue">
                <a className="nav-link nav-link-blue" href="#">Gallery</a>
              </li>
              <li className="nav-item nav-item-blue">
                <Link className="nav-link nav-link-blue nav-link-contact"
                  to="/contact-us">
                  Contact Us
                </Link>
              </li>
              <li className="col-2 nav-item ml-auto nav-partner">
                <div className="nav-link nav-link-partner" href="#">
                  <Link className="btn btn-nav-partner"
                    to="/page/partnerships">
                    PARTNER WITH US
                  </Link>
                </div>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    );
  }
}

/**
 *  map state to props
 * @param {state} state
 * @returns {*} props
 */
const mapStateToProps = ({ authUser, message, user }) => ({
  authUser,
  message,
  user
});

export default compose(connect(mapStateToProps, {
  logout
}), withRouter)(NavBar);
