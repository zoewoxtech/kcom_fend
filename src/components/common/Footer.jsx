import React, { Component } from 'react';

/**
 * @class Footer
 * @extends {Component}
 */
class Footer extends Component {

  /**
   * renders the Footer component
   * @returns {Object} react component to render
   * @memberOf Footer
   */
  render() {
    return (
      <footer className="footer fixed-bottom">
        <div className="container-fluid">
          <div className="container">
            <div className="row">
              <div className="col-sm-3 footer-section">
                <h4>Store</h4>
                <ul className="footer-menu">
                  <li>My Orders</li>
                  <li>Account Profile</li>
                  <li>Link 2</li>
                </ul>
              </div>
              <div className="col-sm-3 footer-section">
                <h4>Daily Digest</h4>
                <ul className="footer-menu">
                  <li>November 2017 (30)</li>
                  <li>October 2017 (31)</li>
                  <li>September 2017 (30)</li>
                  <li>August 2017 (31)</li>
                </ul>
              </div>
              <div className="col-sm-3 footer-section">
                <h4>Quick Links</h4>
                <ul className="footer-menu">
                  <li>About Us</li>
                  <li>Partnership</li>
                  <li>Store</li>
                </ul>
              </div>
              <div className="col-sm-3 footer-section">
                <h4>Our Vision</h4>
                <p className="footer-vision">
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="container-fluid footer-bottom">
          <div className="container">
            <div className="row">
              <div className="col-sm-10 float-left">
                <span className="copyright">
                  © 2013–2017 Kingdom Capstone Outreach Ministry.
                  | Designed by
                  <a href="http://www.zoewox.com"> ZoeWox Technologies</a>.
                </span>
              </div>
              <div className="col-sm-2 float-right footer-socials">
                <div className="row">
                  <span className="">
                    <i className="fab fa-facebook fa-lg" aria-hidden="true"></i>
                  </span>
                  <span className="">
                    <i className="fab fa-twitter fa-lg" aria-hidden="true"></i>
                  </span>
                  <span className="">
                    <i className="fab fa-youtube fa-lg pr-0" aria-hidden="true">
                    </i>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>
    );
  }
}

export default Footer;
