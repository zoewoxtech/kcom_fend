import React, { Component } from 'react';
import { ShareButtons, ShareCounts, generateShareIcon } from 'react-share';

const {
  FacebookShareButton,
  GooglePlusShareButton,
  LinkedinShareButton,
  TwitterShareButton,
  TelegramShareButton,
  WhatsappShareButton,
  PinterestShareButton,
  VKShareButton,
  OKShareButton,
  RedditShareButton,
  TumblrShareButton,
  LivejournalShareButton,
  EmailShareButton
} = ShareButtons;

const {
  FacebookShareCount,
  GooglePlusShareCount,
  LinkedinShareCount,
  PinterestShareCount,
  VKShareCount,
  OKShareCount,
  RedditShareCount,
  TumblrShareCount
} = ShareCounts;

const FacebookIcon = generateShareIcon('facebook');
const TwitterIcon = generateShareIcon('twitter');
const TelegramIcon = generateShareIcon('telegram');
const WhatsappIcon = generateShareIcon('whatsapp');
const GooglePlusIcon = generateShareIcon('google');
const LinkedinIcon = generateShareIcon('linkedin');
const PinterestIcon = generateShareIcon('pinterest');
const VKIcon = generateShareIcon('vk');
const OKIcon = generateShareIcon('ok');
const RedditIcon = generateShareIcon('reddit');
const TumblrIcon = generateShareIcon('tumblr');
const LivejournalIcon = generateShareIcon('livejournal');
const MailruIcon = generateShareIcon('mailru');
const EmailIcon = generateShareIcon('email');

/**
 * @class ShareCountButtons
 * @extends {Component}
 */
export class ShareCountButtons extends Component {

  /**
   * renders the Sidebar component
   * @returns {Object} react component to rend
   */
  render() {
    return (
      <FacebookShareCount url={shareUrl}>
        {shareCount => (
          <span className="myShareCountWrapper">{shareCount}</span>
        )}
      </FacebookShareCount>
    );
  }
}

/**
 * @class ShareIcons
 * @extends {Component}
 */
export class ShareIcons extends Component {

  /**
   * renders the ShareIcon component
   * @returns {Object} react component to render
   * @memberOf Sidebar
   */
  render() {
    return (
      <div className="row justify-content-center">
        <span className="sharebutton">
          <FacebookIcon size={24} round={false} />
        </span>
        <span className="sharebutton">
          <GooglePlusIcon size={24} round={false} />
        </span>
      </div>
    );
  }
}

/**
 * @class ShareButton
 * @extends {Component}
 */
export class ShareButton extends Component {

  /**
   * renders the ShareButton component
   * @returns {Object} react component to rend
   */
  render() {
    const {
      shareUrl, quote, title, description, body
    } = this.props;
    return (
      <div className="row">
        <span className="sharebutton">
          <FacebookShareButton url={shareUrl} quote={title}>
            <FacebookIcon size={28} round />
          </FacebookShareButton>
        </span>
        <span className="sharebutton">
          <TwitterShareButton url={shareUrl} title={title}>
            <TwitterIcon size={28} round />
          </TwitterShareButton>
        </span>
        <span className="sharebutton">
          <GooglePlusShareButton url={shareUrl}>
            <GooglePlusIcon size={28} round />
          </GooglePlusShareButton>
        </span>
        <span className="sharebutton">
          <LinkedinShareButton url={shareUrl} title={title}>
            <LinkedinIcon size={28} round />
          </LinkedinShareButton>
        </span>
        <span className="sharebutton">
          <EmailShareButton url={shareUrl} subject={title} body={body}>
            <EmailIcon size={28} round />
          </EmailShareButton>
        </span>
      </div>
    );
  }
}
