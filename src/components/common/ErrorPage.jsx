import React from 'react';

const ErrorPage = ({ content }) =>
  <div>
    <div className="container main-content">
      <div className="row row-spacing">
        <h1>404 page</h1>
        <p>Text to say that something went awfully wrong</p>
      </div>
      <div className="divider">
      </div>
    </div>
  </div>;

export default ErrorPage;
