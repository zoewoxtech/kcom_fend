import React from 'react';
import { Link } from 'react-router-dom';

import { LoaderSmall } from '../common/Loader';

const RecentCC = ({ recentDevotionals }) => {
  const recentDevo = recentDevotionals.nodes.map(devotional =>
    <div className="col-lg-6" key={devotional.id}>
      <span className="text-justify light-text py-2 px-2">
        { new Date(devotional.publish_date).toDateString() }
        <h5 className="px-2">
          <Link to={`/${recentDevotionals.nodetype}/${devotional.id}`}>
            {devotional.title}
          </Link>
        </h5>
      </span>
    </div>);

  return (
    <div className="col-lg-12">
      <div className="row">
        <h2 className="col-lg-11">Previous Capstone Capsules</h2>
      </div>
      <div className="row">
        <div className="col-lg-8">
          <div className="row py-2">
            {recentDevo}
          </div>
          <Link className="btn btn-outline-primary"
            to={`/${recentDevotionals.nodetype}`}>
            View All
          </Link>
        </div>

        <div className="col-lg-4">
          <img className="img-thumbnail thumbnail float-right"
            src="http://via.placeholder.com/275x225" alt="" />
        </div>
      </div>
    </div>
  );
};

export default RecentCC;
