import React, { Component } from 'react';

/**
 * @class Loader
 * @extends {Component}
 */
export const Loader = () =>
<div className="container main-content">
<div className="row row-spacing">
<div className="loader mx-auto d-block"></div>
</div>
</div>;

/**
 * @class LoaderSmall
 * @extends {Component}
 */
export const LoaderSmall = () =>
  <div className="container main-content">
    <div className="row row-spacing">
      <div className="loader mx-auto d-block"></div>
    </div>
  </div>;
