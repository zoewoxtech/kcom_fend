import React, { Component } from 'react';

const PageHeaders = ({ header }) =>
  <div>
    <div className="page-header">
      <div className="container">
        <h2 className="pt-5">{header}</h2>
      </div>
    </div>
  </div>;

export default PageHeaders;
