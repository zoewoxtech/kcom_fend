import React from 'react';

const Sidebar = ({ sidebar }) =>
  <div className="sidebar-module">
    <h6>Partner With Us</h6>
    <div className="sidebar-module-inset">
      <div className="row">
        <span className="light-text col-sm-4">Bank:</span>
        <span className="col-sm-8">Skye Bank</span>
      </div>
      <div className="row">
        <span className="light-text col-sm-4">Name:</span>
        <span className="col-sm-8">Kingdom Capstone Outreach Ministry</span>
      </div>
      <div className="row">
        <span className="light-text col-sm-4">Account:</span>
        <span className="col-sm-8">1140033326</span>
      </div>
      <div className="sidebar-button my-3">
        <a href="#" className="btn btn-sm btn-primary">Donate</a>
      </div>
    </div>
  </div>;

export default Sidebar;
