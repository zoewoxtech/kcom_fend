import React from 'react';
import { Link } from 'react-router-dom';

const Navbar = () =>
  <div className="admin-navbar">
    <div className="container">
      <ul className="nav justify-content-end">
        <li className="nav-item dropdown">
          <Link className="nav-link dropdown-toggle" to="#"
            id="navbarDropdown" data-toggle="dropdown">
            View Nodes
          </Link>
          <div className="dropdown-menu" aria-labelledby="navbarDropdown">
            <Link className="dropdown-item"
              to="/nodes/blog">
              Blogs
            </Link>
            <Link className="dropdown-item"
              to="/nodes/daily_devotional">
              Capstone Capsules
            </Link>
          </div>
        </li>
        <li className="nav-item">
          <Link className="nav-link" to="/users">
            <i className="fas fa-users"></i>
            View All Users
          </Link>
        </li>
      </ul>
    </div>
  </div>;

export default Navbar;
