import _ from 'lodash';
import react, { Component } from 'react';
import ReactPaginate from 'react-paginate';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import swal from 'sweetalert2';
import toastr from 'toastr';

import AllNodesList from './common/AllNodesList';
import { Loader } from '../common/Loader';
import ErrorPage from '../common/ErrorPage';
import PageHeaders from '../common/PageHeaders';

import { getNodes, deleteNodes, trashNodes } from '../../actions/AdminActions';

import { jsUcFirst } from '../../utils/helper';

swal.setDefaults({
  allowOutsideClick: false,
  confirmButtonClass: 'btn btn-primary',
  cancelButtonClass: 'btn btn-danger',
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  buttonsStyling: true,
  reverseButtons: false,
  focusConfirm: false
});

/**
 * View a paginated list of all site nodes
 * @class AllNodes
 * @extends {Component}
 */
class AllNodes extends Component {
  state = {
    allChecked: false,
    actionToPerform: '',
    limit: 10,
    nodes: [],
    nodesToDelete: {},
    nodeType: '',
    order: 'desc',
    page: 1,
    selectedFilter: '',
    sort: 'publish_date',
    status: ''
  };

  /**
   * @desc handles the triggering of the necessary action
   * @returns {null} returns no value
   */
  componentDidMount = () => {
    $('[data-toggle="tooltip"]').tooltip();
    const { nodeType } = this.props.match.params;
    const {
      limit, order, page, sort
    } = this.state;
    this.setState({ nodeType });
    this.props.getNodes(nodeType, '', limit, page, sort, order)
    .catch(() => this.props.history.push('/error'));
  }

  /**
   * @desc handles triggering of the component re-update
   * @param {any} prevProps
   * @returns {null} returns no value
   */
  componentDidUpdate = (prevProps) => {
    $('[data-toggle="tooltip"]').tooltip();
    if (prevProps.match.params.nodeType !== this.props.match.params.nodeType) {
      const {
        limit, order, page, sort
      } = this.state;
      this.setState({ nodeType: this.props.match.params.nodeType });
      this.props.getNodes(
        this.props.match.params.nodeType,
        '', limit, page, sort, order
      )
      .catch(() => this.props.history.push('/error'));
    }
  }

  /**
   * @desc handles making the call to the API endpoint
   * @param {any} event
   * @returns {null} returns no value
   */
  toggleDelete = (event) => {
    const { name, checked } = event.target;
    this.setState({
      allChecked: checked ? this.state.allChecked : false,
      nodesToDelete: {
        ...this.state.nodesToDelete,
        [name]: checked
      }
    }, () => {
      if (checked) {
        let allChecked = true;
        for (let i = 0; i < this.props.nodes.nodes.length; i += 1) {
          const { id } = this.props.nodes.nodes[i];
          if (!this.state.nodesToDelete[id]) {
            allChecked = false;
            break;
          }
        }

        this.setState({ allChecked });
      }
    });
  }

  /**
   * @desc toggle all users delete
   * @param {any} event
   * @returns {null} returns no value
   */
  /**
   * @desc toggle all users delete
   * @param {any} event
   * @returns {null} returns no value
   */
  toggleAllNodes = (event) => {
    const { allChecked } = this.state;
    this.setState({
      allChecked: !allChecked,
      nodesToDelete: this.props.nodes.nodes.map(node =>
        node.id).reduce((toDelete, id) => {
          toDelete[id] = !allChecked;

          return toDelete;
      }, {})
    });
  }

  getNodesToDelete = () => {
    const { nodesToDelete } = this.state;
    return Object.keys(nodesToDelete).map(node =>
      parseInt(node, 10)).filter(id =>
      nodesToDelete[id]);
  }

  /**
   * @desc toggle all users delete
   * @param {any} event
   * @returns {null} returns no value
   */
  onActionChange = (event) => {
    this.setState({ actionToPerform : event.target.value });
  }

  /**
   * @desc toggle all users delete
   * @param {any} event
   * @returns {null} returns no value
   */
  handleActionChange = (event) => {
    event.preventDefault();
    const { actionToPerform, nodes, nodeType } = this.state;

    if (!actionToPerform) {
      toastr.info('Please select an action to perform');
    } else if (actionToPerform === 'trash' || 'delete') {
      swal({
        title: 'Are you sure?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete selected!',
        cancelButtonText: 'No, cancel!'
      })
      .then((result) => {
        if (result.value) {
          this.props.trashNodes(
            this.getNodesToDelete(),
            nodeType, actionToPerform
          )
          .then(() => {
            toastr.success(this.props.message);
            this.props.getNodes(nodeType, '', limit, page, sort, order)
            .catch(() => this.props.history.push('/error'));
          })
          .catch(() => toastr.error(this.props.message));
        }
      });
    }
  }

  /**
   * @desc toggle all users delete
   * @param {any} event
   * @returns {null} returns no value
   */
  handleFilterChange = (event) => {
    let status = document.getElementById(event.target.id).childNodes;
    const { nodeType } = this.props.match.params;
    const {
      limit, order, page, sort
    } = this.state;
    status = status[0].value;
    this.setState({
      actionToPerform: '',
      nodesToDelete: {},
      selectedFilter: status
    });

    if (status === 'all') {
      status = '';
    }
    this.props.getNodes(nodeType, '', limit, page, sort, order, status);
  }

  /**
   * @desc handles change of the search state
   * @param {any} data
   * @returns {null} returns no value
   */
  handlePageClick = (data) => {
    const { selected } = data;
    const {
      limit, nodeType, order, page, sort
    } = this.state;
    const newPage = Math.ceil(selected) + 1;

    this.setState({ page: newPage }, () => {
      this.props.getNodes(nodeType, '', limit, newPage, sort, order);
    });
  };

  /**
   * Renders the view of the component
   * @returns {Object} react component to render
   * @memberOf AllUsers
   */
  render() {
    let pagination;
    const { authUser, message, nodes } = this.props;
    const {
      actionToPerform, allChecked, nodeType, nodesToDelete, selectedFilter
    } = this.state;

    if (_.isEmpty(nodes) || (!_.isEmpty(nodes)
      && nodes.nodetype !== nodeType)) {
      return <Loader />;
    } else {
      pagination = <ReactPaginate
        previousLabel={<i className="fas fa-chevron-circle-left"></i>}
        nextLabel={<i className="fas fa-chevron-circle-right"></i>}
        breakLabel={<a href="">...</a>}
        breakClassName='break-me'
				pageCount={nodes.pagination.totalPages
          ? nodes.pagination.totalPages : 0}
				marginPagesDisplayed={3}
				pageRangeDisplayed={nodes.pagination.totalPages > 9 ? 10
          : nodes.pagination.totalPages}
				onPageChange={this.handlePageClick}
        containerClassName='pagination justify-content-center'
				pageClassName='page-item'
        pageLinkClassName='page-link'
				nextClassName='page-item next-button'
        previousClassName='page-item'
        previousLinkClassName='page-link'
        nextLinkClassName='page-link'
        disabledClassName='disabled'
        activeClassName={'active'} />;

      const topbar = <div className="container">
        <div className="user-actions-container row d-flex mb-4">
          <div className="p-2 my-1 ml-2">
            <form className="form-inline">
              <div className="input-group">
                <select className="custom-select main-input-append"
                  value={actionToPerform} onChange={this.onActionChange}
                  id="nodeActions">
                  <option value="">Select Actions</option>
                  { selectedFilter === 'trash'
                  ? <option value="delete">Delete</option>
                  : <option value="trash">Trash</option>
                  }
                </select>
                <div className="input-group-append">
                  <button type="button" onClick={this.handleActionChange}
                  className="btn btn-outline-primary input-group-append-button">
                    Apply
                  </button>
                </div>
              </div>
            </form>
          </div>
          <div className="p-2 my-1 ml-2">
            <form className="form-inline">
              <div className="btn-group btn-group-toggle" data-toggle="buttons">
                <label id="all" className="btn btn-secondary active"
                  onClick={this.handleFilterChange}>
                  <input type="radio" value="all" defaultChecked />View All
                </label>
                <label id="published" className="btn btn-secondary"
                  onClick={this.handleFilterChange}>
                  <input type="radio" value="published" />Published
                </label>
                <label id="draft" className="btn btn-secondary"
                  onClick={this.handleFilterChange}>
                  <input type="radio" value="draft" />Draft
                </label>
                <label id="trash" className="btn btn-secondary"
                  onClick={this.handleFilterChange}>
                  <input type="radio" value="trash" />Trash
                </label>
              </div>
            </form>
          </div>
          <div className="ml-auto p-2 my-1 mr-2">
            <form className="form-inline">
              <Link to={`/article/${nodeType}`}
                className="btn btn-primary">
                Create Node
              </Link>
            </form>
          </div>
        </div>
      </div>;

      const content = <div className="container">
        <div className="user-actions-container row">
          <div className="grouped-list-item-check form-check col-sm-1 py-3">
            <div className="grouped-list-item-check-form">
              <input type="checkbox" value="checkbox" checked={allChecked}
                onClick={this.toggleAllNodes} id="deleteCheckbox"
                className="grouped-list-item-checkbox" />
              <span className="checkmark"></span>
            </div>
          </div>
          <div className="big-light-text py-3 col-sm-7 col-lg-7">
            <span className="ml-2">Title</span>
          </div>
          <div className="big-light-text py-3 col-sm-1 col-lg-1">
            Status
          </div>
          <div className="big-light-text py-3 col-sm-2 col-lg-2">
            Publish date
          </div>
          <div className="big-light-text py-3 col-sm-1 col-lg-1">
            Actions
          </div>
        </div>
      </div>;

      const headerText = jsUcFirst(nodeType.replace('_', ' '));

      return (
        <div>
          <PageHeaders header={`${headerText}s`} />
          <div className="container main-content">
            <div className="row row-spacing">
              <div className="nodes-list col-sm-12">
                {topbar}
                {content}
                <div className="grouped-list">
                  <AllNodesList
                    nodes={nodes.nodes} nodesToDelete={nodesToDelete}
                    nodeType={nodeType} formatNodeType={headerText}
                    toggleDelete={this.toggleDelete} />
                </div>
                <nav>
                  {nodes.pagination.totalPages > 1 ? pagination : null}
                </nav>
              </div>
            </div>
          </div>
        </div>
      );
    }
  }
}

/**
 *  map state to props
 * @param {state} state
 * @returns {*} props
 */
const mapStateToProps = ({
  authUser, message, nodes = []
}) => ({
  authUser,
  message,
  nodes
});

export default connect(mapStateToProps, {
  deleteNodes, getNodes, trashNodes
})(AllNodes);
