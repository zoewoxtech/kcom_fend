import _ from 'lodash';
import react, { Component } from 'react';
import ReactPaginate from 'react-paginate';
import { connect } from 'react-redux';
import toastr from 'toastr';
import swal from 'sweetalert2';

import ErrorPage from '../common/ErrorPage';
import { Loader } from '../common/Loader';
import PageHeaders from '../common/PageHeaders';
import Sidebar from '../common/Sidebar';
import RecentCC from '../common/RecentCC';
import UserList from './common/UserList';

import {
  deleteUsers, getUsers, updateUserProfile
} from '../../actions/AdminActions';
import { getDevotionals } from '../../actions/DevotionalActions';
import { jsUcFirst } from '../../utils/helper';

swal.setDefaults({
  allowOutsideClick: false,
  confirmButtonClass: 'btn btn-primary',
  cancelButtonClass: 'btn btn-danger',
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  buttonsStyling: true,
  reverseButtons: false,
  focusConfirm: false
});

/**
 * View a paginated list of all site users
 * @class AllUsers
 * @extends {Component}
 */
class AllUsers extends Component {
  state = {
    hasErrors: false,
    users: [],
    userTitle: '',
    usersToDelete: {},
    sort: 'created_at',
    order: 'asc',
    limit: 10,
    page: 1,
    allChecked: false
  };

  /**
   * @desc handles the triggering of the necessary action
   * @returns {null} returns no value
   */
  componentDidMount = () => {
    if (!this.props.authUser) {
      toastr.error(
        "You don't have permission to visit this page.",
        'Access denied!'
      );
      this.props.history.goBack();
    } else {
      this.apiCall();
    }
  }

  apiCall = () => {
    const {
      limit, sort, order, page
    } = this.state;
    this.props.getUsers('', limit, page, sort, order)
    .catch(() => {
      toastr.error(this.props.message);
      this.setState({
        hasErrors: true
      });
    });

    this.props.getDevotionals(
      'daily_devotional',
      '', 4, 1, 'publish_date', 'desc'
    );
  }

  /**
   * @desc handles making the call to the API endpoint
   * @param {any} user
   * @returns {null} returns no value
   */
  showProfile = (user) => {
    const {
      firstname, lastname, role, username, status, email, bio
    } = user;
    const middlename = user.middlename || '';
    const dateJoined = new Date(user.created_at).toDateString();
    const dob = new Date(user.dob).toDateString();

    swal({
      imageUrl: 'http://via.placeholder.com/200x200',
      imageHeight: 200,
      imageAlt: `${username} image`,
      title: `${lastname}, ${firstname} ${middlename}`,
      width: 600,
      padding: 100,
      html:
        `<div class="divider"></div>
        <div class="row profile-info text-left"><b class="col-sm-12 col-lg-4">Username:</b> ${username}</div>
        <div class="row profile-info text-left"><b class="col-sm-12 col-lg-4">Status:</b> ${status}</div>
        <div class="row profile-info text-left"><b class="col-sm-12 col-lg-4">Email:</b> ${email}</div>
        <div class="row profile-info text-left"><b class="col-sm-12 col-lg-4">Role:</b> ${role} user</div>
        <div class="row profile-info text-left"><b class="col-sm-12 col-lg-4">Date joined:</b> ${dateJoined}</div>
        <div class="row profile-info text-left"><b class="col-sm-12 col-lg-4">Date of birth:</b> ${dob}</div>
        <div class="row profile-info text-left"><b class="col-sm-12 col-lg-4">Bio:</b> ${bio}</div>
        <div class="row-spacing"></div>`,
      allowOutsideClick: true,
      showCloseButton: true,
      showConfirmButton: false
    });
  }

  /**
   * @desc handles making the call to the API endpoint
   * @param {any} event
   * @returns {null} returns no value
   */
  toggleDelete = (event) => {
    const { name, checked } = event.target;
    this.setState({
      allChecked: checked ? this.state.allChecked : false,
      usersToDelete: {
        ...this.state.usersToDelete,
        [name]: checked
      }
    }, () => {
      if (checked) {
        let allChecked = true;
        for (let i = 0; i < this.props.users.length; i += 1) {
          const { username } = this.props.users[i];
          if (!this.state.usersToDelete[username]) {
            allChecked = false;
            break;
          }
        }

        this.setState({ allChecked });
      }
    });
  }

  /**
   * @desc toggle all users delete
   * @param {any} event
   * @returns {null} returns no value
   */
  toggleAllUsers = (event) => {
    const { allChecked } = this.state;
    this.setState({
      allChecked: !allChecked,
      usersToDelete: this.props.users.map(user =>
        user.username).reduce((toDelete, username) => {
          toDelete[username] = !allChecked;

          return toDelete;
      }, {})
    });
  }

  getUsersToDelete = () => {
    const { usersToDelete } = this.state;
    return Object.keys(usersToDelete).filter(username =>
      usersToDelete[username]);
  }

  /**
   * @desc handles making the call to the API endpoint
   * @param {any} event
   * @returns {null} returns no value
   */
  performAction = (event) => {
    event.preventDefault();
    const {
      limit, page, sort, order, usersToDelete
    } = this.state;

    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete selected!',
      cancelButtonText: 'No, cancel!'
    }).then((result) => {
      if (result.value) {
        this.props.deleteUsers(this.getUsersToDelete())
        .then((response) => {
          this.props.getUsers('', limit, page, sort, order);
          swal({
            title: 'Deleted!',
            html: 'Delete action was successful',
            type: 'success'
          });
          this.setState({ allChecked: false, usersToDelete: {} });
        })
        .catch(() => {
          toastr.error('An error occured');
        });
      } else if (result.dismiss === swal.DismissReason.cancel) {
        swal('Cancelled', 'No users deleted :)', 'error');
      }
    });
  }

  /**
   * @desc handles making the call to the API endpoint
   * @param {any} user
   * @returns {null} returns no value
   */
  updateUserProfileModal = async (user) => {
    const { value: formValues } = await swal({
      title: 'Update Profile',
      html: `<form>
          <div class="form-group row">
            <label for="firstname" class="form-label col-sm-4 text-left">Firstname:</label>
            <div class="col-sm-8">
              <input type="text" id="firstname" class="form-control" name="firstname" value="${user.firstname}" required />
            </div>
          </div>
          <div class="form-group row">
            <label for="lastname" class="form-label col-sm-4 text-left">Lastname:</label>
            <div class="col-sm-8">
              <input type="text" id="lastname" class="form-control" name="lastname"value="${user.lastname}" required />
            </div>
          </div>
          <div class="form-group row">
            <label for="middlename" class="form-label col-sm-4 text-left">Middlename:</label>
            <div class="col-sm-8">
              <input type="text" id="middlename" class="form-control" name="middlename"value="${user.middlename}" />
            </div>
          </div>
          <div class="form-group row">
            <label for="dob" class="form-label col-sm-4 text-left">Date of birth:</label>
            <div class="col-sm-8">
              <input type="date" id="dob" class="form-control" name="dob"value="${user.dob}" required />
            </div>
          </div>
          <div class="form-group row">
            <label for="bio" class="form-label col-sm-4 text-left">Short Bio:</label>
            <div class="col-sm-8">
              <textarea class="form-control" id="bio" rows="7" name="bio">${user.bio}</textarea>
            </div>
          </div>
        </form>`,
      confirmButtonText: '<i class="fas fa-thumbs-up"></i>Proceed!',
      confirmButtonAriaLabel: 'Proceed!',
      cancelButtonText: '<i class="fas fa-thumbs-down"></i>Cancel!',
      cancelButtonAriaLabel: 'Cancel',
      showCancelButton: true,
      preConfirm: () => {
        if ($('#firstname').val() && $('#lastname').val() && $('#dob').val()) {
          return {
            firstname: $('#firstname').val(),
            lastname: $('#lastname').val(),
            middlename: $('#middlename').val() || '',
            bio: $('#bio').val() || '',
            dob: $('#dob').val()
          };
        } else {
          return { firstname: '' };
        }
      }
    });

    if (formValues && formValues.firstname && formValues.lastname) {
      swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        confirmButtonText: '<i class="fas fa-thumbs-up"></i>Yes proceed!',
        confirmButtonAriaLabel: 'Yes proceed!',
        cancelButtonText: '<i class="fas fa-thumbs-down"></i>No, cancel!',
        cancelButtonAriaLabel: 'Thumbs down',
        showCancelButton: true
      }).then((result) => {
        if (result.value) {
          this.props.updateUserProfile(user.id, formValues)
          .then((res) => {
            swal({ type: 'success', text: this.props.message });
          })
          .catch((err) => {
            swal({ type: 'error', text: this.props.message });
          });
        } else if (result.dismiss === swal.DismissReason.cancel) {
          swal('Cancelled', 'Update process aborted!', 'error');
        }
      });
    } else {
      swal({
        type: 'error',
        text: 'User update failed/cancelled. Try again!'
      });
    }
  }

  /**
   * @desc handles change of the search state
   * @param {any} data
   * @returns {null} returns no value
   */
  handlePageClick = (data) => {
    const { selected } = data;
    const {
      limit, page, order, sort
    } = this.state;

    this.setState({ page: Math.ceil(selected) + 1 }, () => {
      this.apiCall();
    });
  };

  /**
   * Renders the view of the component
   * @returns {Object} react component to render
   * @memberOf AllUsers
   */
  render() {
    let pagination;
    const {
      message, users, recentDevotionals
    } = this.props;
    const {
      hasErrors, singleUser, usersToDelete, allChecked
    } = this.state;

    if (hasErrors) {
      return (
        <ErrorPage content={message} />
      );
    } else if (_.isEmpty(users) || _.isEmpty(recentDevotionals)) {
      return <Loader />;
    } else {
      pagination = <ReactPaginate
        previousLabel={<i className="fas fa-chevron-circle-left"></i>}
				nextLabel={<i className="fas fa-chevron-circle-right"></i>}
				breakLabel={<a href="">...</a>}
				breakClassName='break-me'
				pageCount={users.pagination.totalPages
          ? users.pagination.totalPages : 0}
				marginPagesDisplayed={3}
				pageRangeDisplayed={users.pagination.totalPages > 9 ? 10
          : users.pagination.totalPages}
				onPageChange={this.handlePageClick}
        containerClassName='pagination justify-content-center'
				pageClassName='page-item'
        pageLinkClassName='page-link'
				nextClassName='page-item next-button'
        previousClassName='page-item'
        previousLinkClassName='page-link'
        nextLinkClassName='page-link'
        disabledClassName='disabled'
        activeClassName={'active'} />;

      const content = <div className="col-sm-12">
        <div className="container">
          <div className="user-actions-container row">
            <div className="grouped-list-item-check form-check col-sm-1">
              <div className="grouped-list-item-check-form">
                <input type="checkbox" value="checkbox" checked={allChecked}
                  onClick={this.toggleAllUsers} id="deleteCheckbox"
                  className="grouped-list-item-checkbox" />
                <span className="checkmark"></span>
              </div>
            </div>
            <form className="form-inline" onSubmit={this.performAction} >
              <div className="form-group">
                <select className="custom-select mx-2" id="usersActions">
                  <option defaultValue>Select Action</option>
                  <option name="activate">Activate User(s)</option>
                  <option name="block">Block User(s)</option>
                  <option name="delete">Delete User(s)</option>
                  <option name="change_role">Change User(s) Role</option>
                </select>
                <button className="btn btn-outline-primary">
                  Apply
                </button>
              </div>
            </form>
          </div>
        </div>
        <div className="grouped-list">
          <UserList
            users={users.users} toggleDelete={this.toggleDelete}
            showProfile={this.showProfile} usersToDelete={usersToDelete}
            showUpdateUserProfileModal={this.updateUserProfileModal}
            deleteUsers={this.deleteUsers} />
        </div>
      </div>;

      return (
        <div>
          <PageHeaders header="All Users" />
          <div className="container main-content">
            <div className="row row-spacing">
              {content}
            </div>
            <nav>
              {users.pagination.totalPages > 1 ? pagination : null}
            </nav>
            <div className="divider">
            </div>
            <div className="row row-spacing">
              <RecentCC recentDevotionals={recentDevotionals} />
            </div>
          </div>
        </div>
      );
    }
  }
}

/**
 *  map state to props
 * @param {state} state
 * @returns {*} props
 */
const mapStateToProps = ({
  authUser, devotionals = [], users = [], message
}) => ({
  authUser,
  message,
  users,
  recentDevotionals: devotionals
});

export default connect(mapStateToProps, {
  deleteUsers, getDevotionals, getUsers, updateUserProfile
})(AllUsers);
