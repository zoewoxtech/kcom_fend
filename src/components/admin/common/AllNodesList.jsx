import React from 'react';
import { Link } from 'react-router-dom';

import { jsUcFirst, truncateText } from '../../../utils/helper';

const AllNodesList = ({
  nodes, nodeType, fomratNodeType, nodesToDelete, toggleDelete
}) => {
  const _nodes = (<ul>
      {nodes.map(node =>
      <li key={node.id} className="grouped-list-item row">
        <div className="grouped-list-item-check form-check col-sm-1 col-lg-1">
          <div className="grouped-list-item-check-form">
            <input
              type="checkbox"
              checked={nodesToDelete[node.id] || false}
              onChange={toggleDelete}
              name={node.id}
              className="grouped-list-item-checkbox"
              id="deleteCheckbox" />
            <span className="checkmark"></span>
          </div>
        </div>
        <div className="col-sm-7 col-lg-7">
          <div className="grouped-list-item-link ml-2">
            <div className="grouped-list-item-content">
              <span>
                <div className="mt-1 pt-1">
                  {truncateText(jsUcFirst(node.title), 50)}
                </div>
              </span>
            </div>
          </div>
        </div>
        <div className="col-sm-1 col-lg-1">
          <div className="grouped-list-item-link">
            <div className="grouped-list-item-content">
              <span>
                <div className="small-light-text mt-1 pt-1">
                  {jsUcFirst(node.status)}
                </div>
              </span>
            </div>
          </div>
        </div>
        <div className="col-sm-2 col-lg-2">
          <div className="grouped-list-item-link">
            <div className="grouped-list-item-content">
              <span>
                <div className="small-light-text mt-1 pt-1">
                {new Date(node.publish_date).toLocaleString().substring(0, 10)}
                </div>
              </span>
            </div>
          </div>
        </div>
        <div className="col-sm-1 col-lg-1">
          <div className="mt-2 pt-4">
            <Link to={`/edit/${nodeType}/${node.slug}`}>
              <i className="fas fa-edit fa-lg text-warning mr-1"
                data-toggle="tooltip" data-placement="bottom"
                title="Edit post">
              </i>
            </Link>
          </div>
        </div>
      </li>)}
    </ul>);

  return _nodes;
};

export default AllNodesList;
