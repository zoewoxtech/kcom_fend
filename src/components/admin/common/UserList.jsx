import React from 'react';
import { Link } from 'react-router-dom';

import { jsUcFirst, truncateText } from '../../../utils/helper';

const UserList = ({
  showProfile,
  showUpdateUserProfileModal,
  toggleDelete,
  usersToDelete,
  users
}) => {
  const _users = (<ul>
      {users.map(user =>
      <li key={user.id} className="grouped-list-item row">
        <div className="grouped-list-item-check form-check col-sm-1">
          <div className="grouped-list-item-check-form">
            <input
              type="checkbox"
              checked={usersToDelete[user.username] || false}
              onChange={toggleDelete}
              name={user.username}
              className="grouped-list-item-checkbox"
              id="deleteCheckbox" />
            <span className="checkmark"></span>
          </div>
        </div>
        <div className="col-sm-10">
          <div className="grouped-list-item-link">
            <div className="grouped-list-item-image"
              onClick={() => { showProfile(user); }}>
              <img className="img-thumbnail"
                src="http://via.placeholder.com/75x75" alt="" />
            </div>
            <div className="grouped-list-item-content">
              <span onClick={() => { showProfile(user); }}>
                <div className="title-text">
                  {jsUcFirst(user.lastname)}, {jsUcFirst(user.firstname)}
                  <span className="small-light-text"> - {user.username}</span>
                </div>
                <div className="small-light-text">
                  {user.bio !== ''
                    ? truncateText(user.bio, 120, '...')
                    : 'No user bio'}
                </div>
              </span>
              <div className="meta">
                <div className="float-left">
                  <span className="box-meta-item-left">
                    {user.role}
                  </span>
                  <span className="box-meta-item-left">
                    {user.status}
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-sm-1">
          <div className="grouped-list-item-actions">
            <a className="box-meta-item-right actions-buttons"
              onClick={() => { showUpdateUserProfileModal(user); }}>
              <i className="far fa-edit fa-md"></i>Edit
            </a>
          </div>
        </div>
      </li>)}
    </ul>);

  return _users;
};

export default UserList;
