import React, { Component } from 'react';
import { connect } from 'react-redux';
import renderHTML from 'react-render-html';
import { Link, withRouter } from 'react-router-dom';
import { compose } from 'redux';
import _ from 'lodash';
import toastr from 'toastr';

import { Loader } from '../common/Loader';
import PageHeaders from '../common/PageHeaders';
import RecentCC from '../common/RecentCC';
import { ShareButton } from '../common/Sharing';
import Sidebar from '../common/Sidebar';

import { getNode, resetNode } from '../../actions/NodesActions';
import { getDevotionals } from '../../actions/DevotionalActions';

import { jsUcFirst } from '../../utils/helper';

/**
 * @class Nodes
 * @extends {Component}
 */
class ViewNode extends Component {
  state = {}

  componentWillMount = () => {
    const { nodeType, nodeId } = this.props.match.params;
    this.props.getDevotionals(
      'daily_devotional',
      '', 4, 1, 'publish_date', 'desc'
    );
    this.props.getNode(nodeId, nodeType)
    .catch(() => {
      toastr.error(this.props.message);
      this.props.history.push('/error');
    });
  }

  /**
   * @desc handles triggering of the component re-update
   * @param {any} nextProps
   * @returns {null} returns no value
   */
  componentWillUnmount = () => {
    this.props.resetNode();
  }

  /**
   * @desc renders the Nodes component
   * @returns {Object} react component to render
   */
  render() {
    const {
      authUser, message, node, recentDevotionals
    } = this.props;
    const { isLoading } = this.state;
    const { nodeId, nodeType } = this.props.match.params;
    if ((_.isEmpty(node) || _.isEmpty(recentDevotionals))
      || (!_.isEmpty(node)
      && !((Number(nodeId) === node.id) || (nodeId === node.slug)))) {
        return <Loader />;
    } else {
      const headerText = node.title;
      return <div>
        <PageHeaders header={headerText} />
        <div className="container main-content">
          <div className="row row-spacing">
            <div className="col-lg-9 col-sm-12">
              <div className="content-container mx-4">
                <div className="clearfix mt-1 mb-1">
                  <div className="float-left">
                    <span className="small-light-text">Date: </span>
                    <span className="normal-light-text font-italic">
                      {new Date(node.publish_date).toDateString()}
                    </span>
                  </div>
                  <div className="float-right">
                    <span>
                    <ShareButton
                      shareUrl={`${window.location.href}`}
                      quote={node.title}
                      title={node.title}
                      description={node.meta.excerpt}
                      body={node.content} />
                    </span>
                  </div>
                </div>
                { nodeType === 'daily_devotional'
                  ? <span>
                      <div className="mt-3">
                        <span className="small-light-text">
                          Bible meditation: </span>
                        <span className="normal-light-text font-italic">
                          {node.meta.bible_meditation}
                        </span>
                      </div>
                    </span>
                : '' }
                <div className="divider"></div>
                <div className="row mx-4 mt-2 mb-4">
                  { nodeType === 'daily_devotional'
                  ? <div className="mt-3 mb-4 text-center font-italic">
                      "{node.meta.memory_verse}"
                    </div>
                  : '' }
                  {renderHTML(node.content)}
                </div>
                <div className="divider"></div>
                { nodeType === 'daily_devotional'
                  ? <span>
                      <div className="mt-3">
                        <span className="small-light-text">Author: </span>
                        <span className="normal-light-text font-italic">
                          {node.meta.author}
                        </span>
                      </div>
                      <div className="text-justify mt-1">
                        <span className="small-light-text">Prayer: </span>
                        <span className="normal-light-text font-italic">
                          {node.meta.prayer}
                        </span>
                      </div>
                    </span>
                  : '' }
                <div className="text-justify mt-1">
                  <span className="small-light-text">Categories: </span>
                  <span className="badge badge-pill badge-info">
                    {node.meta.categories || 'None'}
                  </span>
                </div>
                <div className="text-justify mt-1">
                  <span className="small-light-text">Tags: </span>
                  <span className="badge badge-pill badge-info">
                    {node.meta.tags || 'None'}
                  </span>
                </div>
              </div>
            </div>
            <aside className="col-sm-12 col-lg-3 blog-sidebar">
              { authUser && (authUser.role === 'super_admin' || 'admin')
                ? <Link to={`/edit/${nodeType}/${node.slug}`}
                  className="btn btn-outline-primary mb-4 btn-block">
                  <i className="fas fa-edit fa-lg mr-1"></i>
                  Edit post
                </Link>
                : '' }
            <Sidebar />
            </aside>
          </div>
          <div className="divider">
          </div>
          <div className="row row-spacing">
            <RecentCC recentDevotionals={recentDevotionals} />
          </div>
        </div>
      </div>;
    }
  }
}

/**
 *  map state to props
 * @param {state} state
 * @returns {*} props
 */
const mapStateToProps = ({
  authUser, devotionals = [], message, node
}) => ({
  authUser,
  message,
  node,
  recentDevotionals: devotionals
});

export default
  compose(connect(mapStateToProps, {
    getDevotionals, getNode, resetNode
  }), withRouter)(ViewNode);
