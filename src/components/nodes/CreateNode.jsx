import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { compose } from 'redux';
import toastr from 'toastr';

import { Loader } from '../common/Loader';
import PageHeaders from '../common/PageHeaders';
import { NewNodeForm, NewNodeSidebar } from './common/NewNodeForm';

import { createNode } from '../../actions/NodesActions';

import { jsUcFirst } from '../../utils/helper';

/**
 * @class CreateNode
 * @extends {Component}
 */
class CreateNode extends Component {
  state = {
    categories: {},
    editorValue: '',
    node: {
      'status': '',
      'meta': {
        'status': '',
        'excerpt': '',
        'prayer': '',
        'author': '',
        'bible_meditation': '',
        'memory_verse': ''
      },
      'title': '',
      'content': '',
      'publish_date': ''
    },
    nodeType: '',
    tags: {}
  };

  modules = {
    toolbar: [
      ['bold', 'italic', 'underline', 'strike', 'blockquote'],
      ['link', 'blockquote', 'image'],
      [{ list: 'ordered' }, { list: 'bullet' }],
      [{ 'indent': '-1' }, { 'indent': '+1' }],
      [{ 'header': 1 }, { 'header': 2 }],
      [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
      ['clean']
    ]
  };
  placeholder = 'Compose an epic...';
  scrollingContainer = '#scrolling-container'
  theme = 'snow';
  formats = [
    'header',
    'bold', 'italic', 'underline', 'strike', 'blockquote',
    'list', 'bullet', 'indent',
    'link', 'image'
  ];

  componentDidMount = () => {
    if (!this.props.authUser
      || (['super_admin', 'admin'].indexOf(this.props.authUser.role) < 0)) {
      toastr.error(
        "You don't have permission to visit this page.",
        'Access denied!'
      );
      this.props.history.goBack();
    } else {
      this.setState({ nodeType: this.props.match.params.nodeType });
    }
  }

  /**
   * @desc handles change of the search state
   * @param {any} event
   * @returns {null} returns no value
   */
  updateFormState = (event) => {
    const field = event.target.name;
    const { node } = this.state;

    node[field] = event.target.value;
    return this.setState({ node });
  }

  /**
   * @desc handles change of the search state
   * @param {any} value
   * @returns {null} returns no value
   */
  onEditorChange = (value) => {
    this.setState({ editorValue: value });
  }

  /**
   * @desc handles change of the search state
   * @param {any} event
   * @returns {null} returns no value
   */
  updateFormMeta = (event) => {
    const { value, name } = event.target;
    const meta = this.state.node.meta || {};
    const { node } = this.state;

    meta[name] = value;
    node.meta = meta;
    return this.setState({ node });
  }

  /**
   * @desc handles change of the search state
   * @param {any} event
   * @returns {null} returns no value
   */
  handleChipSelection = (event) => {
  }

  createNewNode = () => {
    const { editorValue, node, nodeType } = this.state;
    node.content = editorValue;
    this.props.createNode(node, nodeType)
    .then(() => {
      toastr.success(this.props.message);
      this.props.history.push(`/articles/${nodeType}`);
    })
    .catch(() => {
      toastr.error(this.props.message);
    });
  }

  /**
   * @desc renders the CreateNode component
   * @returns {Object} react component to render
   */
  render () {
    const { match, node } = this.props;
    const { nodeType } = this.state;

    if (!nodeType || nodeType.length <= 0) {
      return <Loader />;
    } else {
      const headerText = jsUcFirst(nodeType.replace('_', ' '));
      return (
        <div>
          <PageHeaders header={`Create New ${headerText}`} />
          <div className="container main-content">
            <div className="row row-spacing">
              <div className="col-lg-8 col-sm-12">
                <div className="container card">
                  <NewNodeForm
                    editorValue={this.state.editorValue}
                    formats={this.formats}
                    modules={this.modules}
                    node={this.state.node}
                    nodeType={nodeType}
                    onEditorChange={this.onEditorChange}
                    scrollingContainer={this.scrollingContainer}
                    updateFormMeta={this.updateFormMeta}
                    updateFormState={this.updateFormState} />
                </div>
              </div>
              <aside className="col-sm-12 col-lg-4 blog-sidebar">
                <button type="submit" onClick={this.createNewNode}
                  className="btn btn-primary mb-4 btn-block">
                  <i className="fas fa-save"></i>
                  Save Post
                </button>

                <div className="sidebar-module mb-4">
                  <div className="card">
                    <h5 className="card-header bg-secondary">
                      Other Form Info
                    </h5>
                    <div className="card-body m-3">
                      <NewNodeSidebar
                        handleChipSelection={this.handleChipSelection}
                        node={this.state.node}
                        nodeType={nodeType}
                        updateFormMeta={this.updateFormMeta}
                        updateFormState={this.updateFormState} />
                    </div>
                  </div>
                </div>
                <div className="sidebar-module mb-4">
                  <div className="card">
                    <h5 className="card-header bg-secondary">
                      Selected Categories
                    </h5>
                    <div className="card-body m-3">
                      <div className="card-text">
                        <span className="badge badge-info mr-2">
                          Category 1
                        </span>
                        <span className="badge badge-info mr-2">
                          Category 2
                        </span>
                        <span className="badge badge-info mr-2">
                          Category 3
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="sidebar-module mb-4">
                  <div className="card">
                    <h5 className="card-header bg-secondary">Selected Tags</h5>
                    <div className="card-body m-3">
                      <div className="card-text">
                        <span className="badge badge-info mr-2">Tag 1</span>
                        <span className="badge badge-info mr-2">Tag 2</span>
                        <span className="badge badge-info mr-2">Tag 3</span>
                      </div>
                    </div>
                  </div>
                </div>
              </aside>
            </div>

            <div className="divider"></div>
          </div>
        </div>
      );
    }
  }
}

/**
 *  map state to props
 * @param {state} state
 * @returns {*} props
 */
const mapStateToProps = ({
  authUser, message, node, nodeType
}) =>
  ({
    authUser,
    message,
    node,
    nodeType
});

export default
  compose(connect(mapStateToProps, {
    createNode
  }), withRouter)(CreateNode);
