import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import toastr from 'toastr';

import { Loader } from '../common/Loader';
import RecentCC from '../common/RecentCC';
import Sidebar from '../common/Sidebar';

import { getDevotionals } from '../../actions/DevotionalActions';
import PageHeaders from '../common/PageHeaders';

/**
 * Declare and define the contact component in the application
 * @class Contact
 * @extends {Component}
 */
class Contact extends Component {
  /**
   * Creates an instance of Contact.
   * @param {any} props property of component
   * @param {any} context property of component
   * @returns {*} no return value
   * @memberof Contact
   */
  constructor(props, context) {
    super(props, context);
    this.state = {
      formData: {},
      sort: 'publish_date',
      order: 'desc',
      limit: 4
    };

    this.updateFormState = this.updateFormState.bind(this);
  }

  /**
   * @desc handles the triggering of the necessary action
   * @returns {null} returns no value
   */
  componentDidMount() {
    const { limit, sort, order } = this.state;
    this.props.getDevotionals('daily_devotional', '', limit, 1, sort, order);
  }

  /**
   * @desc handles making the call to the API endpoint
   * @param {any} event
   * @returns {null} returns no value
   */
  updateFormState(event) {
    const field = event.target.name;
    const { formData } = this.state;
    formData[field] = event.target.value;
    return this.setState({ formData });
  }

  /**
   * Renders the view of the component
   * @returns {Object} react component to render
   * @memberOf Routes
   */
  render() {
    const { recentDevotionals, message } = this.props;
    if (_.isEmpty(recentDevotionals)) {
      return <Loader />;
    } else {
      return (
        <div>
          <PageHeaders header="Contact Us" />
          <div className="container main-content">
            <div className="row row-spacing">
              <div className="col-lg-8 col-sm-12">
                <div className="container div-bordered">
                  <form className="form-signin justify-content-center">
                    <h5>Reach us here:</h5>
                    <div className="form-group">
                      <div className="row">
                        <div className="col">
                          <label htmlFor="fullname">Fullname</label>
                          <input type="text" id="fullname"
                            className="form-control"
                            onChange={this.updateFormState}
                            aria-describedby="fullnameHelp"
                            placeholder="Fullname"
                            name="fullname" required />
                          <small id="fullnameHelp"
                            className="form-text text-muted">
                          </small>
                        </div>
                      </div>
                    </div>
                    <div className="form-group">
                      <div className="row">
                        <div className="col">
                          <label htmlFor="email">Email</label>
                          <input type="text" className="form-control" id="email"
                            aria-describedby="emailHelp" placeholder="Email"
                            name="email" onChange={this.updateFormState}
                            required />
                          <small id="emailHelp"
                            className="form-text text-muted">
                          </small>
                        </div>
                      </div>
                    </div>
                    <div className="form-group">
                      <div className="row">
                        <div className="col-lg-4 col-sm-12">
                          <label htmlFor="category">Category</label>
                          <select className="form-control" id="category"
                            name="category" onChange={this.updateFormState}>
                            <option>Select One</option>
                            <option value="prayers">Prayer Request</option>
                            <option value="testimonies">Testimonies</option>
                            <option value="others">Others</option>
                          </select>
                        </div>

                        <div className="col-lg-8 col-sm-12">
                          <label htmlFor="subject">Subject</label>
                          <input type="text" className="form-control"
                            aria-describedby="subjectHelp" placeholder="Subject"
                            id="subject" name="subject"
                            onChange={this.updateFormState} required />
                          <small id="subjectHelp"
                            className="form-text text-muted">
                          </small>
                        </div>
                      </div>
                    </div>
                    <div className="form-group">
                      <label htmlFor="message">Message</label>
                      <textarea className="form-control" id="message" rows="6"
                        placeholder="Message" onChange={this.updateFormState}
                        aria-describedby="messageHelp" name="message" required>
                      </textarea>
                      <small id="messageHelp" className="form-text text-muted">
                      </small>
                    </div>
                    <button type="submit"
                      className="btn btn-primary my-2">
                      Submit
                    </button>
                  </form>
                </div>
              </div>
              <aside className="col-sm-12 col-lg-4 blog-sidebar">
                <Sidebar />
              </aside>
            </div>

            <div className="divider"></div>

            <div className="row row-spacing">
              <RecentCC recentDevotionals={recentDevotionals} />
            </div>

            <div className="divider"></div>
          </div>
          <div className="">
            <img className="img"
              src="http://via.placeholder.com/2000x421?text=Insert+map+here"
              alt="" />
          </div>
        </div>
      );
    }
  }
}

/**
 *  map state to props
 * @param {state} state
 * @returns {*} props
 */
const mapStateToProps = ({ authUser, devotionals, message }) => ({
  authUser,
  recentDevotionals: devotionals,
  message
});

export default connect(mapStateToProps, {
  getDevotionals
})(Contact);
