import React, { Component } from 'react';
import ReactPaginate from 'react-paginate';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import { compose } from 'redux';
import toastr from 'toastr';
import _ from 'lodash';

import { Loader } from '../common/Loader';
import NodesList from './common/NodesList';
import PageHeaders from '../common/PageHeaders';
import RecentCC from '../common/RecentCC';
import SearchArea from './SearchArea';
import Sidebar from '../common/Sidebar';

import { getNodes } from '../../actions/NodesActions';
import { getDevotionals } from '../../actions/DevotionalActions';

import { jsUcFirst } from '../../utils/helper';

/**
 * @class Nodes
 * @extends {Component}
 */
class Nodes extends Component {
  /**
   * Creates an instance of Nodes.
   * @param {any} props property of component
   * @param {any} context property of component
   * @returns {*} no return value
   * @memberof Nodes
   */
  constructor(props, context) {
    super(props, context);
    const _pageName = this.props.match.params.pageName;
    this.state = {
      hasErrors: false,
      limit: 10,
      nodetype: _pageName,
      order: 'desc',
      page: 1,
      searchQuery: '',
      sort: 'publish_date'
    };
  }

  /**
   * @desc handles the triggering of the necessary action
   * @returns {null} returns no value
   */
  componentDidMount = () => {
    this.apiCall(this.props.match.params.pageName);
  }

  /**
   * @desc handles triggering of the component re-update
   * @param {any} prevProps
   * @returns {null} returns no value
   */
  componentDidUpdate = (prevProps) => {
    if (prevProps.match.params.pageName !== this.props.match.params.pageName) {
      this.apiCall(this.props.match.params.pageName);
      this.setState({ nodetype: this.props.match.params.pageName });
    }
  }

  /**
   * @desc handles making the call to the API endpoint
   * @param {any} pageName
   * @returns {null} returns no value
   */
  apiCall = (pageName) => {
    const {
      page, limit, searchQuery, order, sort
    } = this.state;
    this.props.getNodes(pageName, searchQuery, limit, page, sort, order)
    .then(() => {
      toastr.success(this.props.message);
      this.setState({
        searchQuery: ''
      });
    })
    .catch(() => {
      toastr.error(this.props.message);
    });

    this.props.getDevotionals(
      'daily_devotional',
      '', 4, 1, 'publish_date', 'desc'
    );
  }

  /**
   * @desc handles change of the search state
   * @param {any} event
   * @returns {null} returns no value
   */
  updateSearchState = (event) => {
    this.setState({ searchQuery: event.target.value });
  }

  /**
   * @desc handles change of the sort state
   * @param {any} event
   * @returns {null} returns no value
   */
  updateSortState = (event) => {
    event.preventDefault();
    if (event.target.name === 'sort') {
      this.setState({ sort: event.target.value });
    }
    if (event.target.name === 'order') {
      this.setState({ order: event.target.value });
    }
  }

  /**
   * @desc handles clearing the search and filter states
   * @param {any} event
   * @returns {null} returns no value
   */
  clearSearchFilter = (event) => {
    event.preventDefault();
    const _pageName = this.props.match.params.pageName;
    this.setState({
      searchQuery: '',
      sort: 'publish_date',
      order: 'desc'
    });
  }

  /**
   * @desc handles change of the search state
   * @param {any} event
   * @returns {null} returns no value
   */
  handleSortSearch = (event) => {
    event.preventDefault();
    this.apiCall(this.props.match.params.pageName);
  }

  /**
   * @desc handles change of the search state
   * @param {any} data
   * @returns {null} returns no value
   */
  handlePageClick = (data) => {
    const { selected } = data;
    const { nodetype } = this.state;

    this.setState({ page: Math.ceil(selected) + 1 }, () => {
      this.apiCall(nodetype);
    });
  };


  /**
   * @desc renders the Nodes component
   * @returns {Object} react component to render
   */
  render() {
    let pagination;
    const {
      authUser, message, nodes, recentDevotionals
    } = this.props;
    const { nodetype } = this.state;
    if (_.isEmpty(nodes) || _.isEmpty(recentDevotionals)
      || (!_.isEmpty(nodes) && nodes.nodetype !== nodetype)) {
      return <Loader />;
    } else if (this.state.hasErrors) {
      return (
        <div>
          <p>{message}</p>
        </div>
      );
    } else {
      pagination = <ReactPaginate
        previousLabel={<i className="fas fa-chevron-circle-left"></i>}
        nextLabel={<i className="fas fa-chevron-circle-right"></i>}
        breakLabel={<a href="">...</a>}
        breakClassName='break-me'
				pageCount={nodes.pagination.totalPages
          ? nodes.pagination.totalPages : null}
				marginPagesDisplayed={3}
				pageRangeDisplayed={nodes.pagination.totalPages > 9 ? 10
          : nodes.pagination.totalPages}
				onPageChange={this.handlePageClick}
        containerClassName='pagination justify-content-center'
				pageClassName='page-item'
        pageLinkClassName='page-link'
				nextClassName='page-item next-button'
        previousClassName='page-item'
        previousLinkClassName='page-link'
        nextLinkClassName='page-link'
        disabledClassName='disabled'
        activeClassName={'active'} />;

      const content = <div className="nodes-list col-lg-9 col-sm-12">
        <SearchArea
          sortSearch={this.handleSortSearch}
          updateSearchState={this.updateSearchState}
          updateSortState={this.updateSortState}
          searchValue={this.state.searchQuery}
          sortValue={this.state.sort}
          nodetype={nodetype}
          orderValue={this.state.order}
          clearSearchFilter={this.clearSearchFilter} />
        <NodesList nodes={nodes.nodes} nodetype={nodes.nodetype} />
      </div>;

      const headerText
        = jsUcFirst(this.props.match.params.pageName.replace('_', ' '));

      return (
        <div>
          <PageHeaders header={headerText} />
          <div className="container main-content">
            <div className="row row-spacing">
              {content}
                <aside className="col-sm-12 col-lg-3 blog-sidebar">
                { authUser && (authUser.role === 'super_admin' || 'admin')
                  ? <Link to={`/article/${nodetype}`}
                      className="btn btn-primary mb-4 btn-block">
                      <i className="fas fa-plus-square"></i>
                    Create Node
                  </Link>
                  : '' }
                  <Sidebar />
                </aside>
            </div>
            <nav>
              {nodes.pagination.totalPages > 1 ? pagination : null}
            </nav>
            <div className="divider">
            </div>
            <div className="row row-spacing">
              <RecentCC recentDevotionals={recentDevotionals} />
            </div>
          </div>
        </div>
      );
    }
  }
}

/**
 *  map state to props
 * @param {state} state
 * @returns {*} props
 */
const mapStateToProps = ({
  authUser, devotionals = [], message, nodes
}) => ({
  authUser,
  message,
  nodes,
  recentDevotionals: devotionals
});

export default
  compose(connect(mapStateToProps, {
    getDevotionals, getNodes
  }), withRouter)(Nodes);
