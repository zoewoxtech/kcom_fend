import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { compose } from 'redux';
import toastr from 'toastr';

import { Loader } from '../common/Loader';
import PageHeaders from '../common/PageHeaders';
import Sidebar from '../common/Sidebar';
import { NewNodeForm, NewNodeSidebar } from './common/NewNodeForm';

import { getNode, editNode } from '../../actions/NodesActions';

import { jsUcFirst } from '../../utils/helper';

/**
 * @class EditNode
 * @extends {Component}
 */
class EditNode extends Component {
  state = {
    categories: {},
    editorValue: '',
    node: {},
    nodeToEdit: this.props.node || {},
    nodeType: '',
    tags: {}
  }

  modules = {
    toolbar: [
      ['bold', 'italic', 'underline', 'strike', 'blockquote'],
      ['link', 'blockquote', 'image'],
      [{ list: 'ordered' }, { list: 'bullet' }],
      [{ 'indent': '-1' }, { 'indent': '+1' }],
      [{ 'header': 1 }, { 'header': 2 }],
      [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
      ['clean']
    ]
  };
  placeholder = 'Compose an epic...';
  scrollingContainer = '#scrolling-container'
  theme = 'snow';
  formats = [
    'header',
    'bold', 'italic', 'underline', 'strike', 'blockquote',
    'list', 'bullet', 'indent',
    'link', 'image'
  ];

  componentWillMount = () => {
    const { authUser } = this.props;
    const { nodeId, nodeType } = this.props.match.params;
    if (!authUser || (['super_admin', 'admin'].indexOf(authUser.role) < 0)) {
      toastr.error(
        "You don't have permission to visit this page.",
        'Access denied!'
      );
      this.props.history.goBack();
    } else {
      this.setState({ nodeType: this.props.match.params.nodeType });
      this.props.getNode(nodeId, nodeType)
      .catch(() => {
        this.props.history.push('/error');
      });
    }
  }

  /**
   * @desc handles change of the search state
   * @param {any} nextProps
   * @returns {null} returns no value
   */
  componentWillReceiveProps = (nextProps) => {
    if (nextProps.node !== this.props.node) {
      if (nextProps.authUser && nextProps.node
        && (nextProps.authUser.id !== nextProps.node.user_id)) {
        toastr.error(
          "You don't have permission to visit this page.",
          'Access denied!'
        );
        this.props.history.goBack();
      }
      this.setState({
        editorValue: nextProps.node.content,
        nodeToEdit: nextProps.node
      });
    }
  }

  /**
   * @desc handles change of the search state
   * @param {any} event
   * @returns {null} returns no value
   */
  updateFormState = (event) => {
    const field = event.target.name;
    const { nodeToEdit } = this.state;

    nodeToEdit[field] = event.target.value;
    return this.setState({ nodeToEdit });
  }

  /**
   * @desc handles change of the search state
   * @param {any} event
   * @returns {null} returns no value
   */
  updateFormMeta = (event) => {
    const { value, name } = event.target;
    const meta = this.state.nodeToEdit.meta || {};
    const { nodeToEdit } = this.state;

    meta[name] = value;
    nodeToEdit.meta = meta;
    return this.setState({ nodeToEdit });
  }

  /**
   * @desc handles change of the search state
   * @param {any} value
   * @returns {null} returns no value
   */
  onEditorChange = (value) => {
    this.setState({ editorValue: value });
  }

  saveNode = () => {
    const { editorValue, nodeToEdit, nodeType } = this.state;
    nodeToEdit.content = editorValue;
    const nodeId = nodeToEdit.slug;
    const fieldsToRemove = [
      'created_at', 'modified_at', 'id', 'node_type_id', 'user_id'
    ];
    for (let i = 0; i < fieldsToRemove.length; i += 1) {
      delete nodeToEdit[fieldsToRemove[i]];
    }
    this.props.editNode(nodeToEdit, nodeId, nodeType)
    .then(() => {
      toastr.success(this.props.message);
      this.props.history.goBack();
    })
    .catch(() => {
      toastr.error(this.props.message);
    });
  }

  /**
   * @desc renders the CreateNode component
   * @returns {Object} react component to render
   */
  render() {
    const { nodeToEdit, nodeType } = this.state;

    if (_.isEmpty(nodeToEdit) || (!nodeType || nodeType.length <= 0)) {
      return <Loader />;
    } else {
      const headerText = `Edit ${jsUcFirst(nodeType.replace('_', ' '))}`;
      return (
        <div>
          <PageHeaders header={`Create New ${headerText}`} />
          <div className="container main-content">
            <div className="row row-spacing">
              <div className="col-lg-8 col-sm-12">
                <div className="container card">
                  <NewNodeForm
                    editorValue={this.state.editorValue}
                    formats={this.formats}
                    modules={this.modules}
                    node={nodeToEdit}
                    nodeType={nodeType}
                    onEditorChange={this.onEditorChange}
                    scrollingContainer={this.scrollingContainer}
                    updateFormState={this.updateFormState}
                    updateFormMeta={this.updateFormMeta} />
                </div>
              </div>
              <aside className="col-sm-12 col-lg-4 blog-sidebar">
                <button type="submit" onClick={this.saveNode}
                  className="btn btn-primary mb-4 btn-block">
                  <i className="fas fa-save"></i>
                  Save Post
                </button>

                <div className="sidebar-module mb-4">
                  <div className="card">
                    <h5 className="card-header bg-secondary">
                      Other Form Info
                    </h5>
                    <div className="card-body m-3">
                      <NewNodeSidebar
                        node={nodeToEdit}
                        nodeType={nodeType}
                        updateFormState={this.updateFormState}
                        updateFormMeta={this.updateFormMeta} />
                    </div>
                  </div>
                </div>
                <div className="sidebar-module mb-4">
                  <div className="card">
                    <h5 className="card-header bg-secondary">
                      Selected Categories
                    </h5>
                    <div className="card-body m-3">
                      <div className="card-text">
                        <span className="badge badge-info mr-2">
                          Category 1
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="sidebar-module mb-4">
                  <div className="card">
                    <h5 className="card-header bg-secondary">Selected Tags</h5>
                    <div className="card-body m-3">
                      <div className="card-text">
                        <span className="badge badge-info mr-2">Tag 1</span>
                      </div>
                    </div>
                  </div>
                </div>
              </aside>
            </div>

            <div className="divider"></div>
          </div>
        </div>
      );
    }
  }
}

/**
 *  map state to props
 * @param {state} state
 * @returns {*} props
 */
const mapStateToProps = ({
  authUser, message, node, nodeType
}) =>
  ({
    authUser,
    message,
    node,
    nodeType
});

export default
  compose(connect(mapStateToProps, {
    getNode, editNode
  }), withRouter)(EditNode);
