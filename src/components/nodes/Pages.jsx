import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import renderHTML from 'react-render-html';
import { compose } from 'redux';
import toastr from 'toastr';

import { Loader } from '../common/Loader';
import RecentCC from '../common/RecentCC';
import Sidebar from '../common/Sidebar';

import { getDevotionals } from '../../actions/DevotionalActions';
import { getNode } from '../../actions/NodesActions';
import PageHeaders from '../common/PageHeaders';

/**
 * @class Pages
 * @extends {Component}
 */
class Pages extends Component {
  /**
   * Creates an instance of pages.
   * @param {any} props property of component
   * @param {any} context property of component
   * @returns {*} no return value
   * @memberof pages
   */
  constructor(props, context) {
    super(props, context);

    this.state = {};
  }

  /**
   * @desc handles the triggering of the necessary action
   * @returns {null} returns no value
   */
  componentWillMount() {
    this.props.getNode(this.props.match.params.pageName, 'page')
    .catch((error) => {
      toastr.error(this.props.message);
      this.props.history.push('/error');
    });

    this.props.getDevotionals(
      'daily_devotional',
      '', 4, 1, 'publish_date', 'desc'
    );
  }

  /**
   * @desc handles triggering of the component re-update
   * @param {any} prevProps
   * @returns {null} returns no value
   */
  componentDidUpdate(prevProps) {
    if (prevProps.match.params.pageName !== this.props.match.params.pageName) {
      this.props.getNode(this.props.match.params.pageName, 'page')
      .catch((error) => {
        toastr.error(this.props.message);
        this.props.history.push('/error');
      });

      this.props.getDevotionals(
        'daily_devotional',
        '', 4, 1, 'publish_date', 'desc'
      );
    }
  }

  /**
   * @desc renders the Nodes component
   * @returns {Object} react component to render
   */
  render() {
    const {
      authUser, message, node, recentDevotionals
    } = this.props;
    if (_.isEmpty(node) || _.isEmpty(recentDevotionals)) {
      return <Loader />;
    } else {
      return <div>
        <PageHeaders header={node.title} />
        <div className="container main-content">
          <div className="row row-spacing">
            <div className="col-lg-9 col-sm-12">
              {renderHTML(node.content)}
              { authUser && (['super_admin'].indexOf(authUser.role) >= 0
                || node.user_id === authUser.id)
              ? <div className="row float-right">
                <Link className="btn btn-sm btn-outline-danger mx-2"
                  to="">
                  Edit page
                </Link>
              </div>
              : '' }
            </div>
            <aside className="col-sm-12 col-lg-3 blog-sidebar">
              <Sidebar />
            </aside>
          </div>
          <div className="divider">
          </div>
          <div className="row row-spacing">
            <RecentCC recentDevotionals={recentDevotionals} />
          </div>
        </div>
      </div>;
    }
  }
}

/**
 *  map state to props
 * @param {state} state
 * @returns {*} props
 */
const mapStateToProps = ({
  devotionals = [], message, node, authUser
}) => ({
  authUser,
  message,
  node,
  recentDevotionals: devotionals
});

export default compose(connect(mapStateToProps, {
  getDevotionals, getNode
}), withRouter)(Pages);
