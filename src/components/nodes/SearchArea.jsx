import React, { Component } from 'react';

/**
 * @class SearchArea
 * @extends {Component}
 */
const SearchArea = ({
  updateSearchState, sortSearch, updateSortState, nodetype,
  searchValue, sortValue, orderValue, clearSearchFilter
}) =>
  <div className="container">
    <div className="user-actions-container row">
      <form className="form-inline col-sm-12 col-lg-5" onSubmit={sortSearch}>
        <div className="form-group">
          <label htmlFor="content-search">Search</label>
          <input
            type="search"
            className="searchbox ml-1"
            id="content-search"
            onKeyUp={updateSearchState} />
        </div>
      </form>
      <form className="form-inline col-sm-12 col-lg-7">
        <div className="form-group">
          <label className="mr-sm-2" htmlFor="content-search">Sort by</label>
          <select className="custom-select mr-sm-2" value={sortValue}
            name="sort" onChange={updateSortState}>
            <option defaultValue>Select one</option>
            <option value={
              nodetype === 'daily_devotional' ? 'publish_date' : 'created_at'
            }>
              Publish Date
            </option>
            <option value="title">Title</option>
            <option value="status">Status</option>
          </select>
        </div>
        <div className="form-group">
          <select className="custom-select mr-sm-1" value={orderValue}
            name="order" onChange={updateSortState} >
            <option defaultValue>Order</option>
            <option value="desc">Descending</option>
            <option value="asc">Ascending</option>
          </select>
        </div>
        <button
          type="submit" onClick={sortSearch}
          className="btn btn-sm btn-outline-primary ml-sm-2 mr-sm-2">
          Sort
        </button>
        <button
          type="submit" onClick={clearSearchFilter}
          className="btn btn-sm btn-outline-danger ml-sm-2 mr-sm-1">
          Clear
        </button>
      </form>
    </div>
  </div>;

export default SearchArea;
