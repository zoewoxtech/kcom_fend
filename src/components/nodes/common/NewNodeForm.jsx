import React, { Component } from 'react';
import ReactQuill from 'react-quill';

export const NewNodeForm = ({
  formats, editorValue, onEditorChange, modules, node, nodeType,
  updateFormMeta, updateFormState, scrollingContainer
}) =>
  <form className="form-signin justify-content-center">
    <div className="form-group">
      <div className="row">
        <div className="col">
          <label htmlFor="title">Title</label>
          <input type="text" id="title" className="form-control"
            onChange={updateFormState} aria-describedby="titleHelp"
            value={node.title} placeholder="Title" name="title" required />
          <small id="titleHelp" className="form-text text-muted"></small>
        </div>
      </div>
    </div>


    <div className="form-group mb-4">
      <label>Content</label>
      <ReactQuill
        value={editorValue}
        onChange={onEditorChange}
        modules={modules}
        scrollingContainer={scrollingContainer}
        formats={formats} />
      <small id="contentHelp" className="form-text text-muted"></small>
    </div>

    <div className="form-group">
      <label htmlFor="excerpt">Excerpt</label>
      <textarea className="form-control" id="excerpt" rows="3"
        value={node.meta.excerpt} placeholder="Excerpt"
        onChange={updateFormMeta} maxLength="300"
        aria-describedby="excerptHelp" name="excerpt" required>
      </textarea>
      <small id="excerptHelp" className="form-text text-muted">
        Max of 400 characters
      </small>
    </div>

    { nodeType === 'daily_devotional'
      ? <div>
        <div className="form-group">
          <div className="row">
            <div className="col">
              <label htmlFor="memory_verse">Key Text:</label>
              <input type="text" id="memory_verse" className="form-control"
                value={node.meta.memory_verse} onChange={updateFormMeta}
                aria-describedby="memory_verseHelp" placeholder="Key Text"
                name="memory_verse" />
              <small id="memory_verseHelp" className="form-text text-muted">
              </small>
            </div>
          </div>
        </div>

        <div className="form-group">
          <div className="row">
            <div className="col">
              <label>Bible Meditation</label>
              <input type="text" id="bible_meditation" className="form-control"
                onChange={updateFormMeta} value={node.meta.bible_meditation}
                name="bible_meditation" placeholder="Bible Meditation"
                aria-describedby="bible_meditationHelp" />
              <small id="bible_meditationHelp" className="form-text text-muted">
              </small>
            </div>
          </div>
        </div>

        <div className="form-group">
          <div className="row">
            <div className="col">
              <label htmlFor="author">Source</label>
              <input type="text" id="author" className="form-control"
                value={node.meta.author} onChange={updateFormMeta}
                aria-describedby="authorHelp" placeholder="Source"
                name="author" />
              <small id="authorHelp" className="form-text text-muted"></small>
            </div>
          </div>
        </div>

        <div className="form-group">
          <div className="row">
            <div className="col">
              <label htmlFor="prayer">Prayer</label>
              <input type="text" id="prayer" className="form-control"
                value={node.meta.prayer} onChange={updateFormMeta}
                aria-describedby="prayerHelp" placeholder="Prayer"
                name="prayer" />
              <small id="prayerHelp" className="form-text text-muted"></small>
            </div>
          </div>
        </div>
      </div>
      : '' }

  </form>;

export const NewNodeSidebar = ({
  handleChipSelection, node, nodeType, updateFormMeta, updateFormState
}) =>
  <div>
    <div className="form-group row">
      <label htmlFor="status"
        className="small-light-text col-sm-3 col-form-label">
        Status
      </label>
      <div className="col-sm-9">
        <select className="form-control" id="status"
          name="status" value={node.status} onChange={updateFormState}>
          <option>Select One</option>
          <option value="draft">Draft</option>
          <option value="published">Published</option>
        </select>
      </div>
    </div>

    <div className="form-group row">
      <label htmlFor="publish_date"
        className="small-light-text col-sm-3 col-form-label">Publish on</label>
      <div className="col-sm-9">
        <input className="form-control" aria-describedby="publish_dateHelp"
          id="publish_date" type="date" name='publish_date' required
          value={node.publish_date.slice(0, 10)} onChange={updateFormState} />
      </div>
    </div>

    <div className="form-group row">
      <label htmlFor="categories"
        className="small-light-text col-sm-3 col-form-label">
        Categories
      </label>
      <div className="col-sm-9">
        <input type="text" id="categories" className="form-control"
          onChange={handleChipSelection} placeholder="Catgories"
          aria-describedby="categoriesHelp" name="categories" required />
        <small id="categoriesHelp" className="form-text text-muted"></small>
      </div>
    </div>

    <div className="form-group row">
      <label htmlFor="tags"
        className="small-light-text col-sm-3 col-form-label">
        Tags
      </label>
      <div className="col-sm-9">
        <input type="text" id="tags" className="form-control"
          onBlur={handleChipSelection} aria-describedby="tagsHelp"
          placeholder="Tags" name="tags" required />
        <small id="tagsHelp" className="form-text text-muted"></small>
      </div>
    </div>
</div>;
