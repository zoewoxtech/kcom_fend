import React from 'react';
import renderHTML from 'react-render-html';
import { Link } from 'react-router-dom';

import { ShareButton } from '../../common/Sharing';

import { jsUcFirst, truncateText } from '../../../utils/helper';

const NodesList = ({ nodes, nodetype }) =>
  nodes.map(node =>
    <div key={node.id} className="row-styling">
      <div className="row" key={node.id}>
        <div className="col-lg-4">
          <img className="img-thumbnail thumbnail"
          src="http://via.placeholder.com/227x170" alt="" />
        </div>
        <div className="col-lg-8 node-list">
          <span className="text-justify light-text">
            <h5 className="">
              <Link to={`/articles/${nodetype}/${node.slug}`}>
                {truncateText(jsUcFirst(node.title), 50)}
              </Link>
            </h5>
            {new Date(node.publish_date).toDateString()}
          </span>
          <div className="text-justify">
            {/* Create the node excerpt field */}
            {renderHTML(truncateText(node.content, 300))}
          </div>
          <div className="row">
            <div className="col-lg-8">
              <ShareButton
                shareUrl={`${window.location.href}/${node.id}`}
                quote={node.title}
                title={node.title}
                description={node.content.substr(0, 60)}
                body={node.content} />
            </div>
            <div className="col-lg-4">
              <Link className="float-right no-btn-link"
              to={`/articles/${nodetype}/${node.id}`}>
                Read More >>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>);

export default NodesList;
