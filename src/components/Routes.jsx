import React from 'react';
import {
  BrowserRouter, Switch, Route
} from 'react-router-dom';
import { Provider } from 'react-redux';

import App from './App';

import HomePage from './home/HomePage';
import Footer from './common/Footer';
import NavBar from './common/NavBar';
import store from '../store';

const Routes = () => (
  <Provider store={store}>
    <BrowserRouter>
      <div>
        <NavBar />
        <Switch>
          <Route exact path="/" component={HomePage}/>
          <App />
        </Switch>
        <Footer/>
      </div>
    </BrowserRouter>
</Provider>);

export default Routes;
