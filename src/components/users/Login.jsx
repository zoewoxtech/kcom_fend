import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import toastr from 'toastr';

import { Loader } from '../common/Loader';
import LoginForm from './common/LoginForm';
import RecentCC from '../common/RecentCC';
import SocialLogin from './common/SocialLogin';

import { getDevotionals } from '../../actions/DevotionalActions';
import { userLogin } from '../../actions/UserActions';

/**
 * Declare and define all routes in the application
 * @class Routes
 * @extends {Component}
 */
class Login extends Component {
  /**
   * Creates an instance of Login.
   * @param {any} props property of component
   * @param {any} context property of component
   * @returns {*} no return value
   * @memberof Login
   */
  constructor(props, context) {
    super(props, context);

    this.state = {
      hasErrors: false,
      user: Object.assign({}, props.user),
      sort: 'publish_date',
      order: 'desc',
      limit: 4
    };

    this.updateFormState = this.updateFormState.bind(this);
    this.login = this.login.bind(this);
  }

  /**
   * @desc handles the triggering of the necessary action
   * @returns {null} returns no value
   */
  componentDidMount() {
    if (!this.props.authUser) {
      const { limit, sort, order } = this.state;
      this.props.getDevotionals('daily_devotional', '', limit, 1, sort, order);
    } else {
      toastr.error(
        'The page you requested is not available.',
        'Access denied!'
      );
      this.props.history.goBack();
    }
  }

  /**
   * @desc handles making the call to the API endpoint
   * @param {any} loginDetails
   * @returns {null} returns no value
   */
  apiCall(loginDetails) {
    this.props.userLogin(loginDetails)
    .then(() => {
      toastr.success(this.props.message);
      this.props.history.push('/');
    })
    .catch(() => {
      toastr.error(this.props.message);
      this.setState({
        hasErrors: true
      });
    });
  }

  /**
   * @desc handles making the call to the API endpoint
   * @param {any} event
   * @returns {null} returns no value
   */
  updateFormState(event) {
    const field = event.target.name;
    const { user } = this.state;
    user[field] = event.target.value;
    return this.setState({ user });
  }

  /**
   * @desc handles making the call to the API endpoint
   * @param {any} event
   * @returns {null} returns no value
   */
  login(event) {
    event.preventDefault();
    this.apiCall(this.state.user);
  }

  /**
   * Renders the view of the component
   * @returns {Object} react component to render
   * @memberOf Login
   */
  render() {
    const { recentDevotionals, message } = this.props;
    if (_.isEmpty(recentDevotionals)) {
      return <Loader />;
    } else {
      const loginForm = <div className="col-lg-8 col-sm-12">
        <div className="container div-bordered">
          <LoginForm
            user={this.state.user}
            login={this.login}
            updateFormState={this.updateFormState} />
        </div>
      </div>;

      return (
        <div>
          <div className="container main-content">
            <div className="row row-spacing">
              {loginForm}
              <aside className="col-sm-12 col-lg-4 blog-sidebar">
                <SocialLogin action="Login" />
              </aside>
            </div>
            <div className="divider">
            </div>
            <div className="row row-spacing">
              <RecentCC recentDevotionals={recentDevotionals} />
            </div>
          </div>
        </div>
      );
    }
  }
}

/**
 *  map state to props
 * @param {state} state
 * @returns {*} props
 */
const mapStateToProps = ({ authUser, devotionals = [], message }) => ({
  authUser,
  recentDevotionals: devotionals,
  message
});

export default connect(mapStateToProps, {
  getDevotionals, userLogin
})(Login);
