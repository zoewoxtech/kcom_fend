import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import swal from 'sweetalert2';
import toastr from 'toastr';

import { Loader } from '../common/Loader';
import PageHeaders from '../common/PageHeaders';

import {
  getUser, updateUserPassword, updateUserProfile
} from '../../actions/UserActions';
import { jsUcFirst } from '../../utils/helper';

swal.setDefaults({
  allowOutsideClick: false,
  confirmButtonClass: 'btn btn-primary',
  cancelButtonClass: 'btn btn-danger',
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  buttonsStyling: true,
  reverseButtons: false,
  focusConfirm: false
});

/**
 * renders the Dashboard component
 * @returns {Object} react component to render
 * @memberOf Dashboard
 */
class Dashboard extends Component {
  state = {};

  /**
   * @desc handles the triggering of the necessary action
   * @returns {null} returns no value
   */
  componentDidMount = () => {
    if (!this.props.authUser) {
      toastr.error(
        "You don't have permission to view the user dashboard.",
        'Access denied!',
        { timeOut: 10000 }
      );
      this.props.history.push('/login');
    } else {
      this.props.getUser(this.props.authUser.id);
    }
  }

  /**
   * @desc handles making the call to the API endpoint
   * @returns {null} returns no value
   */
  updateProfile = async () => {
    const { user } = this.props;
    const { value: formValues } = await swal({
      title: 'Update Profile',
      html: `<form>
          <div class="form-group row">
            <label for="firstname" class="form-label col-sm-4 text-left">Firstname:</label>
            <div class="col-sm-8">
              <input type="text" id="firstname" class="form-control" name="firstname" value="${user.firstname}" required />
            </div>
          </div>
          <div class="form-group row">
            <label for="lastname" class="form-label col-sm-4 text-left">Lastname:</label>
            <div class="col-sm-8">
              <input type="text" id="lastname" class="form-control" name="lastname"value="${user.lastname}" required />
            </div>
          </div>
          <div class="form-group row">
            <label for="middlename" class="form-label col-sm-4 text-left">Middlename:</label>
            <div class="col-sm-8">
              <input type="text" id="middlename" class="form-control" name="middlename"value="${user.middlename}" />
            </div>
          </div>
          <div class="form-group row">
            <label for="dob" class="form-label col-sm-4 text-left">Date of birth:</label>
            <div class="col-sm-8">
              <input type="date" id="dob" class="form-control" name="dob"value="${user.dob}" required />
            </div>
          </div>
          <div class="form-group row">
            <label for="bio" class="form-label col-sm-4 text-left">Short Bio:</label>
            <div class="col-sm-8">
              <textarea class="form-control" id="bio" rows="7" name="bio">${user.bio}</textarea>
            </div>
          </div>
        </form>`,
      confirmButtonText: '<i class="fas fa-thumbs-up"></i>Proceed!',
      confirmButtonAriaLabel: 'Proceed!',
      cancelButtonText: '<i class="fas fa-thumbs-down"></i>Cancel!',
      cancelButtonAriaLabel: 'Cancel',
      showCancelButton: true,
      preConfirm: () => {
        if ($('#firstname').val() && $('#lastname').val()
          && $('#dob').val() && $('#bio').val()) {
          return {
            firstname: $('#firstname').val(),
            lastname: $('#lastname').val(),
            middlename: $('#middlename').val(),
            bio: $('#bio').val(),
            dob: $('#dob').val()
          };
        } else {
          return { firstname: '' };
        }
      }
    });

    if (formValues && formValues.firstname) {
      swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        confirmButtonText: '<i class="fas fa-thumbs-up"></i>Yes proceed!',
        confirmButtonAriaLabel: 'Yes proceed!',
        cancelButtonText: '<i class="fas fa-thumbs-down"></i>No, cancel!',
        cancelButtonAriaLabel: 'Thumbs down',
        showCancelButton: true
      }).then((result) => {
        if (result.value) {
          this.props.updateUserProfile(this.props.authUser.id, formValues)
          .then((res) => {
            swal({ type: 'success', text: this.props.message });
          })
          .catch((err) => {
            swal({ type: 'error', text: this.props.message });
          });
        } else if (result.dismiss === swal.DismissReason.cancel) {
          swal('Cancelled', 'Update process aborted!', 'error');
        }
      });
    } else {
      swal({ text: 'User update cancelled/failed. Try again', type: 'error' });
    }
  }

  /**
   * @desc handles making the call to the API endpoint
   * @returns {null} returns no value
   */
  updatePassword = async () => {
    const { value: formValues } = await swal({
      title: 'Enter new password',
      html: `<form>
          <div class="form-group row">
            <label html="swal-input1" class="form-label col-sm-5 text-left">New Password:</label>
            <div class="col-sm-7">
              <input type="password" id="password" class="form-control" required />
            </div>
          </div>
          <div class="form-group row">
            <label html="swal-input2" class="form-label col-sm-5 text-left">Confirm Password:</label>
            <div class="col-sm-7">
              <input type="password" id="confirmPassword" class="form-control" required />
            </div>
          </div>
        </form>`,
      confirmButtonText: '<i class="fas fa-thumbs-up"></i>Proceed!',
      confirmButtonAriaLabel: 'Proceed!',
      cancelButtonText: '<i class="fas fa-thumbs-down"></i>Cancel!',
      cancelButtonAriaLabel: 'Cancel',
      showCancelButton: true,
      preConfirm: () => {
        if ($('#password').val() && $('#confirmPassword').val()
          && $('#password').val() === $('#confirmPassword').val()) {
          return { password: $('#password').val() };
        } else {
          return { password: '' };
        }
      }
    });

    if (formValues && formValues.password) {
      swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        confirmButtonText: '<i class="fas fa-thumbs-up"></i>Yes proceed!',
        confirmButtonAriaLabel: 'Yes proceed!',
        cancelButtonText: '<i class="fas fa-thumbs-down"></i>No, cancel!',
        cancelButtonAriaLabel: 'Thumbs down',
        showCancelButton: true
      }).then((result) => {
        if (result.value) {
          this.props.updateUserPassword(this.props.authUser.id, formValues)
          .then((res) => {
            swal({ text: 'Password updated successfully!', type: 'success' });
          })
          .catch((err) => {
            swal({ text: this.props.message, type: 'error' });
          });
        } else if (result.dismiss === swal.DismissReason.cancel) {
          swal('Cancelled', 'Password process aborted!', 'error');
        }
      });
    } else {
      swal({ text: 'User update cancelled/failed. Try again', type: 'error' });
    }
  }

  /**
   * Renders the view of the component
   * @returns {Object} react component to render
   * @memberOf Routes
   */
  render() {
    const { user } = this.props;
    if (_.isEmpty(user)) {
      return <Loader />;
    } else {
      const headerText = `Dashboard - ${jsUcFirst(user.lastname)},
        ${jsUcFirst(user.firstname)} ${jsUcFirst(user.middlename)}`;
      return <div>
        <PageHeaders header={headerText} />
        <div className="container main-content">
          <div className="row my-5">
            <div className="col-sm-7 pr-4">
              <div className="card">
                <h3 className="card-header">User Profile</h3>
                <div className="card-body m-3">
                  <dl className="row">
                    <dt className="col-sm-3">Name:</dt>
                    <dd className="col-sm-9">
                      {user.lastname}, {user.firstname} {user.middlename}
                    </dd>

                    <dt className="col-sm-3">Username:</dt>
                    <dd className="col-sm-9">{user.username}</dd>

                    <dt className="col-sm-3">Email:</dt>
                    <dd className="col-sm-9">{user.email}</dd>

                    <dt className="col-sm-3">Role:</dt>
                    <dd className="col-sm-9">{user.role}</dd>

                    <dt className="col-sm-3">User: Status</dt>
                    <dd className="col-sm-9">{user.status}</dd>

                    <dt className="col-sm-3">Date: of Birth</dt>
                    <dd className="col-sm-9">
                      {new Date(user.dob).toDateString()}
                    </dd>

                    <dt className="col-sm-3">Bio:</dt>
                    <dd className="col-sm-9">{user.bio}</dd>
                  </dl>
                  <div className="float-right">
                    <button className="btn btn-outline-primary mx-2"
                      onClick={this.updateProfile} value="profile">
                      <i className="far fa-edit fa-lg"></i>
                      Edit profile
                    </button>
                    <button className="btn btn-outline-danger mx-2"
                      onClick={this.updatePassword} value="password">
                      <i className="fas fa-unlock"></i>
                      Change password
                    </button>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-5 pl-4">
              <div className="card">
                <h3 className="card-header">Subscriptions</h3>
                <div className="card-body m-3">
                  <h5 className="card-title">Special title treatment</h5>
                  <p className="card-text">
                    With supporting text below as a natural
                    lead-in to additional content.
                  </p>
                  <a href="#" className="btn btn-outline-primary">
                    Go somewhere
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div className="row my-5">
            <div className="col-sm-7 pr-4">
              <div className="card">
                <h3 className="card-header bg-secondary">Posts</h3>
                <div className="card-body m-3">
                  <h5 className="card-title">Special title treatment</h5>
                  <p className="card-text">
                    With supporting text below as a natural
                    lead-in to additional content.
                  </p>
                  <a href="#" className="btn btn-outline-primary">
                    Go somewhere
                  </a>
                </div>
              </div>
            </div>
            <div className="col-sm-5 pl-4">
              <div className="card">
                <h3 className="card-header bg-secondary">Comments</h3>
                <div className="card-body m-3">
                  <h5 className="card-title">Special title treatment</h5>
                  <p className="card-text">
                    With supporting text below as a natural
                    lead-in to additional content.
                  </p>
                  <a href="#" className="btn btn-outline-primary">
                    Go somewhere
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>;
    }
  }
}

const mapStateToProps = ({ authUser, message, user }) => ({
  authUser,
  message,
  user
});

export default connect(mapStateToProps, {
  getUser, updateUserPassword, updateUserProfile
})(Dashboard);
