import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import toastr from 'toastr';
import _ from 'lodash';

import { Loader } from '../common/Loader';
import RecentCC from '../common/RecentCC';
import SignupForm from './common/SignupForm';
import SocialLogin from './common/SocialLogin';

import { getDevotionals } from '../../actions/DevotionalActions';
import { userSignup } from '../../actions/UserActions';


/**
 * Declare and define the signup component in the application
 * @class Signup
 * @extends {Component}
 */
class Signup extends Component {
  /**
   * Creates an instance of Signup.
   * @param {any} props property of component
   * @param {any} context property of component
   * @returns {*} no return value
   * @memberof Signup
   */
  constructor(props, context) {
    super(props, context);
    this.state = {
      hasErrors: false,
      user: Object.assign({}, props.user),
      sort: 'publish_date',
      order: 'desc',
      limit: 4
    };

    this.updateFormState = this.updateFormState.bind(this);
    this.signup = this.signup.bind(this);
  }

  /**
   * @desc handles the triggering of the necessary action
   * @returns {null} returns no value
   */
  componentDidMount() {
    if (!this.props.authUser) {
      const { limit, sort, order } = this.state;
      this.props.getDevotionals('daily_devotional', '', limit, 1, sort, order);
    } else {
      toastr.error(
        'The page you requested is not available.',
        'Access denied!'
      );
      this.props.history.goBack();
    }
  }

  /**
   * @desc handles making the call to the API endpoint
   * @param {any} userData
   * @returns {null} returns no value
   */
  apiCall(userData) {
    this.props.userSignup(userData)
    .then(() => {
      toastr.success(this.props.message);
      this.props.history.push('/');
    })
    .catch(() => {
      toastr.error(this.props.message);
      this.setState({
        hasErrors: true
      });
    });
  }

  /**
   * @desc handles making the call to the API endpoint
   * @param {any} event
   * @returns {null} returns no value
   */
  updateFormState(event) {
    const field = event.target.name;
    const { user } = this.state;
    user[field] = event.target.value;
    return this.setState({ user });
  }

  /**
   * @desc handles making the call to the API endpoint
   * @param {any} event
   * @returns {null} returns no value
   */
  signup(event) {
    event.preventDefault();
    this.apiCall(this.state.user);
  }

  /**
   * Renders the view of the component
   * @returns {Object} react component to render
   * @memberOf Routes
   */
  render() {
    const { recentDevotionals, message } = this.props;
    if (_.isEmpty(recentDevotionals)) {
      return <Loader />;
    } else {
      const signupForm = <div className="col-lg-8 col-sm-12">
        <div className="container div-bordered">
          <SignupForm
            user={this.state.user}
            signup={this.signup}
            updateFormState={this.updateFormState} />
        </div>
      </div>;

      return (
        <div>
          <div className="container main-content">
          <div className="row row-spacing">
              {signupForm}
              <aside className="col-sm-12 col-lg-4 blog-sidebar">
                <SocialLogin action="Signup" />
              </aside>
            </div>
            <div className="divider">
            </div>
            <div className="row row-spacing">
            <RecentCC recentDevotionals={recentDevotionals} />
            </div>
          </div>
        </div>
      );
    }
  }
}

/**
 *  map state to props
 * @param {state} state
 * @returns {*} props
 */
const mapStateToProps = ({ authUser, devotionals, message }) => ({
  authUser,
  recentDevotionals: devotionals,
  message
});

export default connect(mapStateToProps, {
  getDevotionals, userSignup
})(Signup);
