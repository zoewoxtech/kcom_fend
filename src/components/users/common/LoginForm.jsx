import React, { Component } from 'react';

/**
 * renders the LoginForm
 * @returns {Object} react component to render
 * @memberOf LoginForm
 */
const LoginForm = ({ login, updateFormState, user }) =>
  <form onSubmit={login}
    className="form-signin justify-content-center">
    <div className="form-group">
      <label htmlFor="inputEmail">Email/Username</label>
      <input type="text" className="form-control" id="inputEmail"
        aria-describedby="emailHelp" placeholder="Email or username"
        name="userId" onChange={updateFormState} required />
      <small id="emailHelp" className="form-text text-muted">
        We'll never share your email with anyone else.
      </small>
    </div>
    <div className="form-group">
      <label htmlFor="inputPassword">Password</label>
      <input type="password" className="form-control" id="inputPassword"
        aria-describedby="passwordHelp" placeholder="Password"
        name="password" onChange={updateFormState} required />
        <small id="passwordHelp" className="form-text text-muted">
          Your password must be 8-20 characters long, contain letters
          and numbers, and must not contain spaces, special
          characters, or emoji.
        </small>
    </div>
    <button type="submit"
      className="btn btn-primary">
      Submit
    </button>
  </form>;

export default LoginForm;
