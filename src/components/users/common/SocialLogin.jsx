import React from 'react';

const SocialLogin = ({ action }) =>
  <div className="sidebar-module py-5">
    <h5 className="text-center">{action} With Social Media</h5>
    <div className="sidebar-button">
      <div className="row justify-content-center py-4 px-2">
        <button type="submit" className="btn btn-primary">
          Facebook {action}
        </button>
      </div>
      <div className="row justify-content-center py-4">
        <button type="submit" className="btn btn-danger">
          Google {action}
        </button>
      </div>
    </div>
  </div>;

export default SocialLogin;
