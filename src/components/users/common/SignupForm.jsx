import React, { Component } from 'react';

/**
 * renders the SignupForm component
 * @returns {Object} react component to render
 * @memberOf SignupForm
 */
const SignupForm = ({ signup, updateFormState, user }) =>
  <form onSubmit={signup}
    className="form-signin justify-content-center">
    <div className="form-group">
      <div className="row">
        <div className="col">
          <label htmlFor="firstname">Firstname</label>
          <input type="text" className="form-control" id="firstname"
            aria-describedby="firstnameHelp" placeholder="Firstname"
            name="firstname" onChange={updateFormState} required />
          <small id="firstnameHelp" className="form-text text-muted">
          </small>
        </div>
      </div>
    </div>
    <div className="form-group">
      <div className="row">
        <div className="col">
          <label htmlFor="lastname">Lastname</label>
          <input type="text" className="form-control" id="lastname"
            aria-describedby="lastnameHelp" placeholder="Lastname"
            name="lastname" onChange={updateFormState} required />
          <small id="lastnameHelp" className="form-text text-muted">
          </small>
        </div>
      </div>
    </div>
    <div className="form-group">
      <div className="row">
        <div className="col-md-8">
          <label htmlFor="middlename">Middlename</label>
          <input type="text" className="form-control" id="middlename"
            aria-describedby="middlenameHelp" placeholder="Middlename"
            name="middlename" onChange={updateFormState} />
          <small id="middlenameHelp" className="form-text text-muted">
          </small>
        </div>
        <div className="col-md-4">
          <label htmlFor="dob">Date of Birth</label>
          <input className="form-control" aria-describedby="dobHelp"
            id="dob" type="date" name="dob" required
            onChange={updateFormState} />
          <small id="dobHelp" className="form-text text-muted">
          </small>
        </div>
      </div>
    </div>
    <div className="form-group">
      <div className="row">
        <div className="col">
          <label htmlFor="username">Username</label>
          <input type="text" className="form-control" id="username"
            aria-describedby="usernameHelp" placeholder="Username"
            name="username" onChange={updateFormState} required />
          <small id="usernameHelp" className="form-text text-muted">
          </small>
        </div>
      </div>
    </div>
    <div className="form-group">
      <div className="row">
        <div className="col">
          <label htmlFor="email">Email</label>
          <input type="email" className="form-control" id="email"
            aria-describedby="emailHelp" placeholder="Email"
            name="email" onChange={updateFormState} required />
          <small id="emailHelp" className="form-text text-muted">
          </small>
        </div>
      </div>
    </div>
    <div className="form-group">
      <div className="row">
        <div className="col">
          <label htmlFor="password">Password</label>
          <input type="password" className="form-control" id="password"
            aria-describedby="passwordHelp" placeholder="Password"
            name="password" onChange={updateFormState} required />
          <small id="passwordHelp" className="form-text text-muted">
            Your password must be 8-20 characters long, contain letters
            and numbers, and must not contain spaces, special
            characters, or emoji.
          </small>
        </div>
      </div>
    </div>
    <div className="form-group">
      <div className="row">
        <div className="col">
          <label htmlFor="confirmPassword">Confirm Password</label>
          <input type="password" className="form-control" id="confirmPassword"
            name="confirmPassword" onChange={updateFormState}
            aria-describedby="confirmPasswordHelp"
            placeholder="Confirm Password" required />
          <small id="confirmPasswordHelp" className="form-text text-muted">
            This should match the password entered
          </small>
        </div>
      </div>
    </div>
    <div className="form-group">
      <label htmlFor="bio">Bio</label>
      <textarea className="form-control" id="bio" rows="5"
        placeholder="Bio" onChange={updateFormState}
        aria-describedby="bioHelp" name="bio">
      </textarea>
      <small id="bioHelp" className="form-text text-muted">
      </small>
    </div>
    <button type="submit"
      className="btn btn-primary">
      Submit
    </button>
  </form>;

export default SignupForm;
