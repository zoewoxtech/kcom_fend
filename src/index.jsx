import { render } from 'react-dom';

import { setDefaultHeader } from './utils/helper';

import Routes from './components/Routes';

if (localStorage.getItem('kcomjwt')) {
  setDefaultHeader(localStorage.getItem('kcomjwt'));
}

const appRoot = document.getElementById('react-app');

render(
  <Routes />,
  appRoot
);
