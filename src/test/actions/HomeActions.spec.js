import expect from 'expect';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import moxios from 'moxios';
import nock from 'nock';

import * as HomeActions from '../../actions/HomeActions';
import {
  SUCCESS_MESSAGE,
  ERROR_MESSAGE,
  WELCOME_MESSAGE
} from '../../actions/ActionTypes.js';

const { apiUrl } = process.env;

let expectedActions, store;
const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('The', () => {
  beforeEach(() => moxios.install());
  afterEach(() => moxios.uninstall());

  describe('getOneNode action', () => {
    it('should display the welcome message appropriately', () => {
      moxios.stubRequest(`${apiUrl}node/page/1`, {
        status:200,
        response: { status: 'success', data: {} }
      });
      expectedActions = [
        {
          type: WELCOME_MESSAGE,
          payload: { status: 'success', data: {} }
        }
      ];
      store = mockStore({});
      return store.dispatch(HomeActions.getOneNode('page', 1))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });
});
