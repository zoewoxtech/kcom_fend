import config from './config';

module.exports = {
  'Go to home page': (browser) => {
    browser
      .url(config.url)
      .waitForElementVisible('body')
      .pause(10000)
      .assert.title('KCOM')
      .end();
  }
};
