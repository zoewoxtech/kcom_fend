import React from 'react';
import expect, { createSpy, spyOn, isSpy } from 'expect'
import { HomePage } from '../../components/home/HomePage';

let wrapper;
let mountWrapper;
let props;

/**
 * @desc handles the triggering of the necessary action
 * @param {*} isAuthenticated
 * @param {*} documents
 * @param {*} user
 * @returns {null} returns no value
 */
function setup(homepageContent) {
  props = {
    homepageContent,
    getOneNode: spy(() => new Promise((resolve) => { resolve(); })),
    getDevotionals: spy(() => new Promise((resolve) => { resolve(); })),
    getNodes: spy(() => new Promise((resolve) => { resolve(); })),
    message: ''
  };
  
  return mount(<HomePage {...props} />);
}

/**
 * @desc handles the triggering of the necessary action
 * @param {*} isAuthenticated
 * @param {*} user
 * @param {*} documents
 * @returns {null} returns no value
 */
function shallowSetup(homepageContent) {
  props = {
    homepageContent,
    getOneNode: spy(() => new Promise((resolve) => { resolve(); })),
    getDevotionals: spy(() => new Promise((resolve) => { resolve(); })),
    getNodes: spy(() => new Promise((resolve) => { resolve(); })),
    message: ''
  };
  
  const context = { router: [] };

  return shallow(<HomePage {...props} />, { context });
}

describe('Homepage Component', () => {
  const homepageContent = 'test content';
  wrapper = setup(homepageContent);
  mountWrapper = shallowSetup(homepageContent);

  it('should exist', () => {
    const wrapper = shallow(<div className="some-class" />);
    expect(wrapper.exists()).toEqual(true);
  });
});
