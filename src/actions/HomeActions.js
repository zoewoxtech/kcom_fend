import axios from 'axios';

import * as types from './ActionTypes';

const { apiUrl } = process.env;

export const passSuccessMessage = successMessage =>
  ({ type: types.SUCCESS_MESSAGE, successMessage });

export const passErrorMessage = errorMessage =>
  ({ type: types.ERROR_MESSAGE, errorMessage });

export const getWelcomeMsgSuccess = welcomeMessage =>
  ({ type: types.WELCOME_MESSAGE, payload: welcomeMessage });

export const getNodesSuccess = (data, nodetype) =>
  ({ type: types.NODES, payload: { nodes: data, nodetype } });

const resolveErrors = (error) => {
  if (error.response) {
    return error.response.data;
  } else if (error.request) {
    return 'An error occured with your request, please try again later.';
  } else {
    return error;
  }
};

export const getOneNode = (nodeType, nodeId) => dispatch =>
  axios.get(`${apiUrl}node/${nodeType}/${nodeId}`)
    .then((response) => {
      dispatch(getWelcomeMsgSuccess(response.data));
    })
    .catch((error) => {
      dispatch(passErrorMessage(resolveErrors(error)));
    });


export const getNodes = (
    nodetype = 'page', search = '', limit = 10, offset = 1,
    sort = 'created_at', order = 'desc'
  ) =>
    dispatch =>
      axios.get(`${apiUrl}node/${nodetype}?search=${search}&limit=${limit}&page=${offset}&sort=${sort}&order=${order}`)
        .then((response) => {
          dispatch(getNodesSuccess(response.data.data.nodes, nodetype));
        })
        .catch((error) => {
          dispatch(passErrorMessage(resolveErrors(error)));
        });
