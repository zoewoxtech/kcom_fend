import axios from 'axios';

import * as types from './ActionTypes';

const { apiUrl } = process.env;

export const passSuccessMessage = successMessage =>
  ({ type: types.SUCCESS_MESSAGE, successMessage });

export const passErrorMessage = errorMessage =>
  ({ type: types.ERROR_MESSAGE, errorMessage });

export const getDevotionalsSuccess = (data, nodetype) =>
  ({ type: types.DEVOTIONALS, payload: { nodes: data, nodetype } });

const resolveErrors = (error) => {
  if (error.response) {
    // The request was made and the server responded with a status code
    // that falls out of the range of 2xx
    return error.response.data.message;
  } else if (error.request) {
    // The request was made but no response was received
    // `error.request` is an instance of XMLHttpRequest in the browser and an
    // instance of http.ClientRequest in node.js
    return 'An error occured with your request, please try again later.';
  } else {
    // Something happened in setting up the request that triggered an Error
    return error;
  }
};

export const getDevotionals =  (
    nodetype = 'page', search = '', limit = 10, offset = 1,
    sort = 'created_at', order = 'desc'
  ) =>
    dispatch =>
      axios.get(`${apiUrl}node/${nodetype}?search=${search}&limit=${limit}&page=${offset}&sort=${sort}&order=${order}`)
        .then((response) => {
          dispatch(getDevotionalsSuccess(response.data.data.nodes, nodetype));
        })
        .catch((error) => {
          dispatch(passErrorMessage(resolveErrors(error)));
        });
