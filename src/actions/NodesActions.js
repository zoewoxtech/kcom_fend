import axios from 'axios';

import * as types from './ActionTypes';

const { apiUrl } = process.env;

export const passSuccessMessage = successMessage =>
  ({ type: types.SUCCESS_MESSAGE, successMessage });

export const passErrorMessage = errorMessage =>
  ({ type: types.ERROR_MESSAGE, errorMessage });

export const resetNodeState = () => ({ type: types.RESET_NODE });

export const getNodesSuccess = (data, nodetype) =>
  ({ type: types.NODES, payload: { data, nodetype } });

export const createNodeSuccess = (data, nodetype) =>
  ({ type: types.CREATE_NODE, payload: { node: data } });

export const getNodeSuccess = (data, nodetype) =>
  ({ type: types.NODE, payload: { node: data, nodetype } });

const resolveErrors = (error) => {
  if (error.response) {
    return error.response.data.data.message;
  } else if (error.request) {
    return 'An error occured with your request, please try again later.';
  } else {
    return error;
  }
};

export const getNodes = (
    nodetype = 'page', search = '', limit = 10, offset = 1,
    sort = 'created_at', order = 'desc'
  ) =>
    dispatch =>
      axios.get(`${apiUrl}node/${nodetype}?search=${search}&limit=${limit}&page=${offset}&sort=${sort}&order=${order}`)
        .then((response) => {
          dispatch(getNodesSuccess(response.data.data, nodetype));
          dispatch(passSuccessMessage(response.data.data.message));
        })
        .catch((error) => {
          throw dispatch(passErrorMessage(resolveErrors(error)));
        });

export const getNode = (nodeId, nodetype = 'page') => dispatch =>
  axios.get(`${apiUrl}node/${nodetype}/${nodeId}`)
    .then((response) => {
      dispatch(getNodeSuccess(response.data.data.node, nodetype));
    })
    .catch((error) => {
      throw dispatch(passErrorMessage(resolveErrors(error)));
    });

export const createNode = (node, nodeType) => dispatch =>
  axios.post(`${apiUrl}node/access/${nodeType}`, node)
    .then((response) => {
      dispatch(passSuccessMessage(response.data.data.message));
    })
    .catch((error) => {
      throw dispatch(passErrorMessage(resolveErrors(error)));
    });

export const editNode = (node, nodeId, nodeType) => dispatch =>
  axios.put(`${apiUrl}node/${nodeType}/${nodeId}`, node)
    .then((response) => {
      dispatch(passSuccessMessage(response.data.data.message));
    })
    .catch((error) => {
      throw dispatch(passErrorMessage(resolveErrors(error)));
    });

export const resetNode = () => dispatch => dispatch(resetNodeState());
