import axios from 'axios';

import * as types from './ActionTypes';

import {
  setLocalstorage, setDefaultHeader, checkAuth, clearLocalStorage
} from '../utils/helper';

const { apiUrl } = process.env;

export const passSuccessMessage = successMessage =>
  ({ type: types.SUCCESS_MESSAGE, successMessage });

export const passErrorMessage = errorMessage =>
  ({ type: types.ERROR_MESSAGE, errorMessage });

export const getUserSuccess = user =>
  ({ type: types.USER, user });

export const setCurrentUser = user =>
  ({ type: types.LOGGEDIN_USER, payload: user });

const resolveErrors = (error) => {
  if (error.response) {
    return error.response.data.data.message;
  } else if (error.request) {
    return 'An error occured with your request, please try again later.';
  } else {
    return error;
  }
};

export const userLogin = (loginDetails) => dispatch =>
  axios.post(`${apiUrl}login`, loginDetails)
    .then((response) => {
      const { token } = response.data.data;
      setLocalstorage('kcomjwt', token);
      setDefaultHeader(token);
      dispatch(setCurrentUser(checkAuth()));
      dispatch(passSuccessMessage(response.data.data.message));
    })
    .catch((error) => {
      throw dispatch(passErrorMessage(resolveErrors(error)));
    });

export const logout = () => dispatch =>
  axios.get(`${apiUrl}logout`)
    .then((response) => {
      clearLocalStorage();
      dispatch(setCurrentUser(null));
      dispatch(passSuccessMessage(response.data.data.message));
    })
    .catch((error) => {
      throw dispatch(passErrorMessage(resolveErrors(error)));
    });

export const userSignup = (userData) => dispatch =>
  axios.post(`${apiUrl}signup`, userData)
    .then((response) => {
      const { token } = response.data.data;
      setLocalstorage('kcomjwt', token);
      setDefaultHeader(token);
      dispatch(setCurrentUser(checkAuth()));
      dispatch(passSuccessMessage(response.data.data.message));
    })
    .catch((error) => {
      throw dispatch(passErrorMessage(resolveErrors(error)));
    });

export const getUser = (userId) => dispatch =>
  axios.get(`${apiUrl}users/${userId}`)
    .then((response) => {
      dispatch(getUserSuccess(response.data.data.user));
    })
    .catch((error) => {
      throw dispatch(passErrorMessage(resolveErrors(error)));
    });

export const updateUserPassword = (userId, userData) => dispatch =>
  axios.put(`${apiUrl}users/${userId}`, userData)
    .then((response) => {
      dispatch(passSuccessMessage(response.data.data.message));
    })
    .catch((error) => {
      throw dispatch(passErrorMessage(resolveErrors(error)));
    });

export const updateUserProfile = (userId, userData) => dispatch =>
  axios.put(`${apiUrl}users/${userId}`, userData)
    .then((response) => {
      dispatch(passSuccessMessage(response.data.data.message));
      dispatch(getUserSuccess(response.data.data.user));
    })
    .catch((error) => {
      throw dispatch(passErrorMessage(resolveErrors(error)));
    });
