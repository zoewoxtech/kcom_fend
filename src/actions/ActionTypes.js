export const SUCCESS_MESSAGE = 'SUCCESS_MESSAGE';
export const ERROR_MESSAGE = 'ERROR_MESSAGE';

export const DEVOTIONALS = 'DEVOTIONALS';
export const WELCOME_MESSAGE = 'WELCOME_MESSAGE';
export const CREATE_NODE = 'CREATE_NODE';
export const NODES = 'NODES';
export const NODE = 'NODE';
export const RESET_NODE = 'RESET_NODE';

export const ADMIN_UPDATE_USER = 'ADMIN_UPDATE_USER';
export const LOGGEDIN_USER = 'LOGGEDIN_USER';
export const USERS = 'USERS';
export const USER = 'USER';
