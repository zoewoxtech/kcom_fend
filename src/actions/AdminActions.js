import axios from 'axios';

import * as types from './ActionTypes';
import { getNodesSuccess } from './NodesActions';

const { apiUrl } = process.env;

export const passSuccessMessage = successMessage =>
  ({ type: types.SUCCESS_MESSAGE, successMessage });

export const passErrorMessage = errorMessage =>
  ({ type: types.ERROR_MESSAGE, errorMessage });

export const getUsersSuccess = data =>
  ({ type: types.USERS, payload: { data } });

export const adminUpdateUserSuccess = user =>
  ({ type: types.ADMIN_UPDATE_USER, payload: user });

const resolveErrors = (error) => {
  if (error.response) {
    return error.response.data.data.message;
  } else if (error.request) {
    return 'An error occured with your request, please try again later.';
  } else {
    return error;
  }
};

export const getUsers = (
    search = '', limit = 10, page = 1,
    sort = 'created_at', order = 'asc'
  ) => dispatch =>
  axios.get(`${apiUrl}users?search=${search}&limit=${limit}&page=${page}&sort=${sort}&order=${order}`)
      .then((response) => {
        dispatch(getUsersSuccess(response.data.data));
      })
      .catch((error) => {
        throw dispatch(passErrorMessage(resolveErrors(error)));
      });

export const deleteUsers = (users) => dispatch =>
  axios.delete(`${apiUrl}users`, {
    data: { users }
  })
    .then((response) => {
      dispatch(passSuccessMessage(response.data.data.message));
    })
    .catch((error) => {
      throw dispatch(passErrorMessage(resolveErrors(error)));
    });

export const updateUserProfile = (userId, userData) => dispatch =>
  axios.put(`${apiUrl}users/${userId}`, userData)
    .then((response) => {
      dispatch(passSuccessMessage(response.data.data.message));
      dispatch(adminUpdateUserSuccess(response.data.data.user));
    })
    .catch((error) => {
      throw dispatch(passErrorMessage(resolveErrors(error)));
    });

export const getNodes = (
  nodetype = 'page', search = '', limit = 10, page = 1,
  sort = 'created_at', order = 'desc', status = ''
) =>
  dispatch =>
    axios.get(`${apiUrl}node/access/${nodetype}?search=${search}&limit=${limit}&page=${page}&sort=${sort}&order=${order}&status=${status}`)
      .then((response) => {
        dispatch(getNodesSuccess(response.data.data, nodetype));
      })
      .catch((error) => {
        throw dispatch(passErrorMessage(resolveErrors(error)));
      });

export const trashNodes = (nodes, nodeType, nodeAction) => dispatch =>
  axios.patch(`${apiUrl}node/${nodeType}`, { nodeAction, nodes })
    .then((response) => {
      dispatch(passSuccessMessage(response.data.data.message));
    })
    .catch((error) => {
      throw dispatch(passErrorMessage(resolveErrors(error)));
    });
