import parser from 'body-parser';
import express from 'express';
import path from 'path';
import webpack from 'webpack';

import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import webpackConfig from './webpack.config';


const port = process.env.PORT || 8080;

const app = express();

app.use(parser.urlencoded({ extended: true }));
app.use(parser.json());

if (process.env.NODE_ENV !== 'test') {
  /*eslint-disable global-require*/
  const config = require('./webpack.config');
    const compiler = webpack(config);
    app.use(webpackDevMiddleware(compiler, {
      noInfo: true,
      hot: true,
      publicPath: '/'
    }));
    app.use(webpackHotMiddleware(compiler));
}

app.use('/public', express.static(path.join(__dirname, './public/')));

app.get('/*', (req, res) => res.status(200)
  .sendFile(path.resolve(__dirname, './public/index.html')));

app.listen(port, () => {
  console.log(`Server started on ${port}`);
});

module.exports = app;
