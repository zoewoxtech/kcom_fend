const DotEnvPlugin = require('dotenv-webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path');
const webpack = require('webpack');

const CSSPlugin = new ExtractTextPlugin('style.bundle.css');
const AddedPlugin = new webpack.ProvidePlugin({
  'React': 'react',
  'ReactDOM': 'react-dom'
});

const dotEnvPlugin = new DotEnvPlugin({
  path: './.env'
});

module.exports = {
  devtool: 'eval',
  entry: [
    'webpack-hot-middleware/client',
    'babel-polyfill',
    path.resolve(__dirname, 'src/index')
  ],
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: 'app.bundle.js',
    publicPath: process.env.NODE_ENV === 'development' ? `http://localhost:${+process.env.PORT}/public/` : '/public/'
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        loaders: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader?url=false'
      },
      {
        test: /\.scss$/,
        loader: 'style-loader!css-loader?url=false!sass-loader'
      },
      {
        test: /\.(jpe?g|png|gif)$/i,
        loader: 'file-loader?name=/img/[name].[ext]'
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx', '.css']
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    CSSPlugin,
    AddedPlugin,
    dotEnvPlugin
  ]
};
